#Routing Documentation


##Administration

> /staff/:person_id 
Loads the staff member page

> /staff/
Loads the staff page entirely

## Careers
> /careers/
Loads the careers page

> /careers/:department/:jobPostingId
Loads the job page

> /careers/me
Loads the persons individual career data

> /careers/login
Login to the career portal

> /careers/register
Register to the career portal

> /careers/me/:submissionId
The current application status of a persons submission

## Policy

> /policy/
Loads all of the policies

> /policy/:policy_id
Loads the current policy

## Press

> /press/
Loads the page for all press releases

> /press/releases/:releaseId
loads the press release

> /press/contact
Contact the press department

## EBB

> /ebb/
Landing page EBB Portal

> /ebb/appeal/login
Login to a EBB appeal account

> /ebb/appeal/register
Create a EBB appeal account

> /ebb/appeal/submit
File a EBB appeal

> /ebb/appeal/view
View the status of a current submission

> /ebb/lookup/:robloxusername
Perform a EBB lookup of a person

## CMS

> /cms/login
Login to the WH CMS System, there will be an account created first, and then the user will change their password.

**NOTE**
Rating Levels for pages:

A - All CMS Access
B - Press
C - Policy Advisor 
D - Cabinet
E - Career/Job recruiter
F - WHCOS
G - VPOTUS/POTUS
H - EBB Appeal Viewers

### Careers

> /cms/careers/ [E]
List all careers, based off a users department

> /cms/careers/create [E]
Create a new job posting

> /cms/careers/delete/:careerID [E] [POST]
Delete a job posting

> /cms/careers/:careerID [E]
View a current job posting

> /cms/careers/:careerID/:submissionID [E]
View a current application

> /cms/careers/:careerID/:submissionID/trash [E] [POST]
Deny and Trash an application.

> /cms/careers/:careerID/:submissionID/comment [E] [POST]
Comment on an application

> /cms/careers/:careerID/:submissionID/interview [E] [POST]
Set the application to interview stage

> /cms/careers/:careerID/:submissionID/accept [E] [POST]
Accepts a current job posting

### Administration

> /cms/staff/view [F]
View all current staff

> /cms/staff/add [F] 
Add a new staff member to the Administration page

> /cms/staff/:staffID/delete [F] [POST]
Remove a member from the Administration page

### Policy

> /cms/policy/view [C]
View all current policy in a better table

> /cms/policy/add [C]
Create a new policy

> /cms/policy/:policyID/delete [C] [POST]
Delete a policy

### Press

> /cms/press/view [B]
View all current press releases

> /cms/press/add [B]
Create a new press release

> /cms/press/:pressID/delete [B] [POST]
Delete a press release

### EBB

> /cms/blacklists/view [G]
View all Executive branch blacklists

> /cms/blacklsts/issue [G]
Issue a new blacklist

> /cms/blacklists/:blacklistID/delete [G] [POST]
Delete a blacklist

> /cms/blacklists/appeals/ [H]
View all blacklist appeals

> /cms/blacklists/appeals/:appealID/ [H]
View a current appeal

> /cms/blacklists/appeals/:appealID/comment [H] [POST]
Comment on a appeal

> /cms/blacklists/appeals/:appealID/reccomend [H] [POST]
Reccomend a blacklist be revoked

> /cms/blacklists/appeals/:appealID/trash [H] [POST]
Delete a blacklist and deny it.

### Users

> /cms/users/ [G]
View all users 

> /cms/users/:userId/delete [G] [POST]
Remove a user

> /cms/users/:userId/ [G]
View a current user

> /cms/users/:userId/perms [G] [POST]
Update User Permissions

### Departments

> /cms/departments/ [G]
List all EB Departments

> /cms/departments/:departmentId/delete [G] [POST]
Delete a department and release all users from that department.

> /cms/departments/create [G]
Create a new department for populating members.

