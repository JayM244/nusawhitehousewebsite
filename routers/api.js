api = {};

api.SAVCode = "kl234h2jk3h42l3kb23hblhjcbhj324rl34tkjwerghe";

function makeid(length) {
    var result           = '';
    var characters       = '0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
 function makeToken(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }

var express = require('express')
var router = express.Router()
var bodyParser = require('body-parser')
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var database;
var profileManager = require('../data/profile');
var ebb = require('../admin/data/ebb')
var bodyParser = require('body-parser');
var urlencodedParser = bodyParser.urlencoded({ extended: false })


MongoClient.connect(url, function(err, db) {
  if (err) throw err;
    database = db.db("whsite");
  });

router.get('/:sav/ezlogin/verify/:uid/:username/', function(req,res){
    userid = req.params.uid;
    sav = req.params.sav;
    username = req.params.username;
    code = makeid(6);
    token = makeToken(40);
    if (sav == api.SAVCode)
    {
        toInsert = {Username: username, UserId:userid,Code:code, Token:token};
        database.collection("verify").insertOne(toInsert, function(err,resp){
            if (err)
            {
                res.send(JSON.stringify({status:"fail", message:"Internal Server Error"}));
                return;
            }
            res.send(JSON.stringify({status:"success", message:code}));
        })
    }else
    {
        res.send(JSON.stringify({status:"fail", message:"Internal Authentication Error"}));
    }
});

router.get('/ezlogin/verify/:code', function(req, res){
    code = req.params.code;
    database.collection("verify").findOne({Code:code}, function(err,result){
        error=false;
        if (err) error=true;
        if (result == undefined)
        {
            res.send(JSON.stringify({status:"fail",message:"Invalid Code"}));
            return;
        }
        profileManager.AddToken(result.Token, result.Username, result.UserId)
        database.collection("verify").deleteOne({Code:code}, function(ero,res){
            if (ero) error=true;
        });
        
        if (error)
        {
            res.send(JSON.stringify({status:"fail",message:"Internal Server Error"}));
            return;
        }
        res.cookie("token", result.Token,{ expires: new Date(Date.now() + 604800000)});
        res.send(JSON.stringify({status:"success", message:result.Token, username:result.Username}));
    });
});

router.get("/ezlogin/:token", function(req,res){
    token = req.params.token;
    profileManager.GetProfileFromToken(token, function(profile){
        res.send(JSON.stringify({username: profile.Username}));
    });
    
});

var TicketTypes = {
    3440081399: 0,
    3440083683:1,
    3440084300:2
}
var TypesToList = [
    "5d2b7e1ecf1af541485bba09",
    "5d2b7e2095c4d8264b27817a",
    "5d2b7e240bc64c3588792b75"
];
var request = require("request");

var LabelTypes = {
    Restored: "5d2b9001daf4e84957d84713",
    Game: "5d2b80797bcaf36607ce802c"
}

function doesExist(userid, cb)
{
    database.collection("ticketing").findOne({roblox:userid}, (err,res)=>{
        if (res)
        {
            cb(true)
        }else
        {
            cb(false)
        }
    });
}

router.get("/ticketing/create", function(req,res){
  
    auth = req.query.auth;
    if (auth != "CSrCzDGDgSPQRaTmuvzhSNXkspGUbrApyvFcYRpveBAsZWMbXDvDvcMvwULFasDP")
    {
        res.status(401).send("401 Unauthorized");
        return;
    }
    
    userid = req.query.userid
    username = req.query.username
    type = req.query.type
    restore = req.query.restore
    uselabel = LabelTypes.Game
    if (doesExist(userid, (r)=>{
        if (!r)
        {
            if (restore == "true")
            {
                uselabel = LabelTypes.Restored
            }
            if (type != 3440081399 && type != 3440083683 && type != 3440084300)
            {
                res.status(500).send("Invalid Ticket Types");
                return;
            }
            type_small = TicketTypes[type];
            database.collection("ticketing").insertOne({roblox:userid, type:type_small}, (err,resx)=>{
                res.send(resx.ops[0]._id);
                options = {
                    method: 'POST',
                    url: 'https://api.trello.com/1/cards',
                    qs: {
                      name: `${username}:${userid}`,
                      desc: `CONF: ${resx.ops[0]._id}`,
                      pos: 'top',
                      idList: TypesToList[type_small],
                      idLabels: uselabel,
                      key: 'bb43855ca459ac2bc4a52291c8285b8f',
                      token: '4184a8a862ff6de0a7235c9df64e77c9ad399581f22c0e1733975e177a25e4dc'
                    }
                };
                request(options, function (error, response, body) {
                    if (error) throw new Error(error);
                });
            });
        }else
        {
            res.send("Purchase was OK, but was not proccessed due to you already having a ticket");
        }

    }));
});

var Max =55;
var TicketingEnabled = true;
router.get('/ticketing', function(req,res){
    database.collection("ticketing").find({}).toArray((err,resx)=>{
        if (TicketingEnabled && resx.length < Max)
        {
            res.send("true")
        }else
        {
            res.send("false")
        }
    });
});


//EBB


router.get('/ebb', (req,res)=>{
    user = req.query.userid;
    if (user != undefined)
    {
        ebb.GetBlacklist(user, (cb)=>{
            if (cb == undefined)
            {
                res.send(JSON.stringify({blacklisted:false}));

            }else
            {
                cb.referenceId = cb._id;
                cb._id = undefined;
                res.send(JSON.stringify({blacklisted:true, details:cb}));
            }
        });
    }
});

var pmanager = require('../admin/data/permissions')

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}
router.post('/question-banks',urlencodedParser, (req,res)=>{
    key = req.query.key;
    if (!IsJsonString(req.body.data))
    {
        res.send(JSON.stringify({status:"400", details:"Malformed Request"}));
        return 
    }
    bank = JSON.parse(req.body.data);
    pmanager.AuthAPI(key, (valid)=>{
        if (!valid)
        {
            res.send(JSON.stringify({status:"401", details:"Unauthorized"}));
            return;
        }
        if (bank.name && bank.questions)
        {
            valid = true;
            for(i=0;i<bank.questions.length;i++){
                if (bank.questions[i].type && bank.questions[i].question)
                {

                }else
                {
                    valid = false;
                }
            }
            if (!valid)
            {
                res.send(JSON.stringify({status:"400", details:"Question bank was malformed."}));
                return;
            }
            database.collection("question_banks").insertOne(bank, (err,ress)=>{
                //console.log(ress);
                res.send(JSON.stringify({status:"200", details:{name:bank.name, id:ress.ops[0]._id, fullname:`${bank.name} (${ress.ops[0]._id})`, questions:bank.questions.length}}));
            });
        }else
        {
            res.send(JSON.stringify({status:"400", details:"Malformed Request"}));
        }

    })
});



module.exports = router;