var express = require('express')
var router = express.Router()
var Core = require('../components/core');
var Careers = require('../components/careers/careers');
var Apps = require('../components/careers/applications');
var bodyParser = require('body-parser');
var dataManager = require('../data/careers');
var profileManager = require('../data/profile');
// middleware that is specific to this router
var cookieParser = require('cookie-parser');
var moment = require('moment');
// define the home page route


var urlencodedParser = bodyParser.urlencoded({ extended: false })

async function buildRow(data)
{
    
}

router.use(cookieParser());
router.get('/me',function(req,res){
    //console.log(req);
    if (req.cookies.token)
    {
        
        profileManager.GetProfileFromToken(req.cookies.token, function(result){
            rblx = result.Username;
//		console.log(rblx);
            dataManager.GetUserApplications(rblx, async function(resu){
                row_body = "";
//		console.log(resu)
                if (resu.length > 0)
                {
                   
                    for(i=0;i<resu.length;i++)
                    {
                        current = resu[i];
                       
                        processing = true;
                        

                        jobb = await dataManager.Async.getJobPostingById(current.JobPostingId);

                        job= jobb.JobName

                        depr = await dataManager.Async.getDepartmentById(current.DepartmentId);

                        //console.log(depr);
                        depart = depr.DepartmentName;
                        submitTime = moment(current.PostingTime);
                        submitTime.format('MMM Do H:m A z');
                        actionTime = moment(current.LastActionTime);
                        actionTime.format('MMM Do H:m A z');
                        row_body += Apps.BuildViewRow(current._id, depart, job, current.Status, submitTime, actionTime);
                    }
                    row_body = Apps.BuildViewTable(row_body);
                    doc = Core.BuildMeta("Careers");
                    doc += Core.BuildNavigation();
                    doc += Core.BuildPageHeader("Application Status");
                    doc += Core.BuildBody(row_body);
                    doc += Core.BuildFooter();        
                    res.send(doc);
                    
                }else
                {
                    row_body = Apps.BuildEmptyRow();
                    row_body = Apps.BuildViewTable(row_body);
                    doc = Core.BuildMeta("Careers");
                    doc += Core.BuildNavigation();
                    doc += Core.BuildPageHeader("Application Status");
                    doc += Core.BuildBody(row_body);
                    doc += Core.BuildFooter();
                    res.send(doc);
                }
                
            })
        });
    }else
    {
        doc = Core.BuildMeta("Careers");
        doc += Core.BuildNavigation();
        doc += Core.BuildPageHeader("Application Status - Login");
        doc += Core.BuildBody(Apps.BuildViewLogin());
        doc += Core.BuildFooter();
        res.send(doc);
    }
});
router.post('/me',urlencodedParser, function(req,res){
    code = req.body.lgn_code;
    profileManager.VerifyCode(code, function(token){
        if (token)
        {
            res.cookie("token", token, { expires: new Date(Date.now() + 604800000)});
            res.redirect("/careers/me");
            
        }else
        {
            res.redirect("/careers/me");
            res.send();
        }
        
    });
});
router.get('/', function (req, res) {
    dataManager.getDepartments(function(results){
        body = "";
        departments = "";
        for(i=0;i<results.length;i++)
        {
            current = results[i];
            if (current.hidden != "true")
            {
                departments += Careers.BuildDepartment(current.DepartmentImage, current.DepartmentName, current.Description, current._id);
            }
            
        }
        body += Careers.BuildDepartmentalContainer(departments);
        doc = Core.BuildMeta("Careers");
        doc += Core.BuildNavigation();
        doc += Core.BuildPageHeader("Careers and Applications");
        doc += Core.BuildBody(body);
        doc += Core.BuildFooter();
        res.send(doc);
    });
});
router.get('/:department', function(req, res){
    department = req.params.department;
    
    dataManager.getJobPostings(department, function(results){
        jobs = "";
        //console.log(results);
        for(i=0;i<results.length;i++)
        {
            current = results[i];
            
            jobs += Careers.BuildJob(current.JobName, current.JobDescription, current._id, department, current.vanished);
            
            
            
        }
        dataManager.getDepartmentById(department, function(result){
            body = "";
            body = Careers.BuildDepartmentalContainer(jobs);
            doc = Core.BuildMeta("Careers");
            doc += Core.BuildNavigation();
            doc += Core.BuildPageHeader("Careers - " + result.DepartmentName);
            doc += Core.BuildBody(body);
            doc += Core.BuildFooter();
         res.send(doc);
        });
        
    });

    
});

resolver = {text:0,large:1,section:2}


router.get('/:department/:jobid', function(req,res){
    department = req.params.department;
    jobid = req.params.jobid;
 
    dataManager.getJobApplication(department,jobid,function(result){
        fields = "";
        for(i=0;i<result.Questions.length;i++)
        {
            current = result.Questions[i];
            fields += Apps.BuildFormField(resolver[current.type], current.question, i);
        }
        special = ""
        if (req.query.special == "true")
        {
            special = `<div class="alert alert-info" role="alert">
            This is a special application. There will be additional data that will be autofilled when you submit this application. 
          </div>`
        }
        if (req.query == undefined)
        {
            req.query = {};
        }
        fields = Apps.BuildApplicationForm(result.DepartmentName, result.JobName, fields, special, req.query);
        body = Apps.BuildStepper() + Apps.BuildSubmissionBase(Apps.EZLogin() + fields, department, jobid);
        doc = Core.BuildMeta("Careers");
        doc += Core.BuildNavigation();
        doc += Core.BuildPageHeader("Careers - Application");
        doc += Core.BuildBody(body);
        doc += Core.BuildFooter();
        res.send(doc);
    });
});

router.post('/:department/:jobid',urlencodedParser, function(req,res){
    department = req.params.department;
    jobid = req.params.jobid;
    dataManager.SubmitJob(req.body, department,jobid, function(err){
        body = "";
        if (err)
        {
            body = Apps.BuildStepper_Fail() + Apps.BuildFailurePage(err);
        }else
        {
            body = Apps.BuildStepper_Complete() + Apps.BuildSubmissionPage();
        }

        doc = Core.BuildMeta("Careers");
        doc += Core.BuildNavigation();
        doc += Core.BuildPageHeader("Careers - Application");
        doc += Core.BuildBody(body);
        doc += Core.BuildFooter();
        res.send(doc);
    });
});
//console.log("Hello");

// define the about route


module.exports = router