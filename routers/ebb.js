var express = require('express')
var router = express.Router()
var Core = require('../components/core');
var bodyParser = require('body-parser');

// middleware that is specific to this router
var cookieParser = require('cookie-parser');
var moment = require('moment');
// define the home page route
var ebbComp = require('../components/ebb/ebb')
var ebbdata = require('../data/ebb')
urlencodedParser = bodyParser.urlencoded({ extended: false });


router.get('/', (err,res)=>{
    doc = Core.BuildMeta("EBB");
    doc += Core.BuildNavigation();
    doc += Core.BuildPageHeader("EBB - Lookup");
    doc += Core.BuildBody(ebbComp.BuildLookup());
    doc += Core.BuildFooter();
    res.send(doc)
});

router.post('/', urlencodedParser, (req,resx)=>{
    doc = Core.BuildMeta("EBB");
    doc += Core.BuildNavigation();
    doc += Core.BuildPageHeader("EBB Lookup - " + req.body.username);
    ebbdata.GetBlacklists(req.body.username, (res)=>{
        tbody = ""
        if (res.length == 0)
        {
            tbody = ebbComp.BuildEmptyRow();
        }else{
            for (i=0;i<res.length;i++)
            {
                cur = res[i];
                actionTime = moment(cur.submitTime);
                actionTime.format('MMM Do H:m A z');
                tbody += ebbComp.BuildViewRow(cur._id, cur.tier, actionTime, cur.submitter);
            }
        }
        doc += Core.BuildBody(ebbComp.BuildViewTable(tbody));
        doc += Core.BuildFooter();
        resx.send(doc);
    });

});


module.exports = router;