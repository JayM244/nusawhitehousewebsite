var express = require('express')
var router = express.Router()
var Core = require('../components/core');
var Apps = require('../components/documents/documents');
var bodyParser = require('body-parser');
var dataManager = require('../data/documents');

router.get("/", (req,res)=>{
    dataManager.GetDocuments((docs) =>{
        row_body = "";
        for (i=0;i<docs.length;i++)
        {
           
            cur = docs[i];
            //console.log(cur.Category);
            row_body += Apps.BuildResult(cur.Image, cur.Name,cur.Category.Icon, cur.Category.Name, cur.Category.Color, cur.Description, cur._id, cur.Date, cur.Submitter)
        }
       
        row_body = Apps.BuildContainer(row_body);
        doc = Core.BuildMeta("Documents");
        doc += Core.BuildNavigation();
        doc += Core.BuildPageHeader("Documents");
        doc += Core.BuildBody(row_body);
        doc += Core.BuildFooter();      
        res.send(doc);
    });
     
});
router.get("/:id", (req,res)=>{
    docid = req.params.id;
    dataManager.GetDocument(docid, (ress)=>{
        row_body = Apps.BuildDocumentPage(ress.Link);
        doc = Core.BuildMeta("Documents");
        doc += Core.BuildNavigation();
        doc += Core.BuildPageHeader(ress.Name);
        doc += Core.BuildBody(row_body);
        doc += Core.BuildFooter();    
        res.send(doc);
    });
});


module.exports = router;

