var express = require('express');
var app = express();
var careers = require('./routers/careers');
var api = require('./routers/api');
var admin = require('./admin/admin');
var docs = require('./routers/documents');
var https = require("https");
var fs = require("fs");
const Sentry = require('@sentry/node');
const bodyParser= require('body-parser')
var fileUpload = require('express-fileupload');
var ebb = require('./routers/ebb');
app.use(bodyParser.urlencoded({extended: true}))

//app.use(fileUpload());

Sentry.init({ dsn: 'https://736f302740b04db1836eab2d220e88a1@sentry.io/1484340' });

app.use(Sentry.Handlers.errorHandler());

app.use(express.static('public'))
process.on('uncaughtException', function (err) {
    console.log('Caught exception: ', err);
});


app.use('/documents/', docs);

app.use('/careers/', careers);

app.use('/api/', api);

app.use('/admin/', admin);

app.use('/ebb', ebb)

app.set('trust proxy', true)



app.get('/', function(req,res){
    res.redirect("/careers/");
    res.send();
});


https.createServer({
    key: fs.readFileSync('./key.pem'),
    cert: fs.readFileSync('./cert.pem'),
}, app)
.listen(443);