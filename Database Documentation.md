#Database Documentation
**Database Name** whsite

##Verify

* UserId (ROBLOX UserID)
* Username (ROBLOX Username)
* Code (Verification Codes)
* Token (Preverification Token)

##Profile

* Applications
    * ApplicationId
* Token
* Username

##Applications

* Questions
    * QuestionType
    * QuestionText
* Answers
    * Answer
* DepartmentId
* JobPostingId
* Status
* Comments
     * Name
     * Text
     * Time
* PostingTime
* LastActionTime
* Submitter

##Job Postings

* DepartmentId
* JobPostingId
* QuestionBankId
* JobName
* JobDescription

##Departments

* DepartmentId
* Description
* DepartmentImage



