var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var database;
var mongo = require('mongodb');
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
      database = db.db("whsite");
});
var docs = {}

docs.getCategory = async function(id)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    category = await database.collection("categories").findOne({_id: oid});
    return category;
}


docs.GetDocuments = async function(cb){

    database.collection("publishings").find({List:"true"}).toArray(async (err,pubs)=>{
     
        for (i=0;i<pubs.length;i++)
        {
        
            cur = pubs[i];
            
            catt = await docs.getCategory(cur.Category);
   
            pubs[i].Category = catt;
        }
        
        cb(pubs);
       
    });
}
docs.GetDocument =function(id, cb)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("publishings").findOne({_id: oid}, (err,res)=>{
        cb(res);
    });
}
module.exports = docs;