var Cdata = {}
var MongoClient = require('mongodb').MongoClient;
var mongo = require('mongodb');
var profile = require('./profile');
var url = "mongodb://localhost:27017/";
var request = require('request')
var database;
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
      database = db.db("whsite");
});
  

Cdata.getDepartmentById =function(id, callback)
{
     if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("departments").findOne({_id:oid}, function(err,res){
        if (err) throw err;
        callback(res);
    });
}
Cdata.getJobPostingById =function(did, id, callback)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("postings").findOne({DepartmentId:did, _id:oid}, function(err,res){
        if (err) throw err;

        callback(res);
    });
}

Cdata.Async = {}

Cdata.Async.getDepartmentById = async function(id)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    let rtn = await database.collection("departments").findOne({_id:oid});
    return rtn;
}

Cdata.Async.getJobPostingById = async function(id)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    let rtn = await database.collection("postings").findOne({_id:oid});
    
    return rtn;
}

Cdata.getDepartments =function(callback)
{
    database.collection("departments").find({}).toArray(function(err,result){
        if (err) throw err;
        callback(result);
    });
}

Cdata.getJobPostings =function(dep, callback)
{
    database.collection("postings").find({DepartmentId:dep}).toArray(function(err,res){
        if (err) throw err;
        callback(res);
    });
}

Cdata.getJobApplication = function(dep,job, callback)
{
    //First get the name of the job
    Cdata.getJobPostingById(dep, job, function(result){

        jname = result.JobName;
        qbank = result.QuestionBankId;
        
        //Get the department
        Cdata.getDepartmentById(dep, function(resulta){
            departmentName = resulta.DepartmentName;
            //Get the questions
            oid = new mongo.ObjectID(qbank);
            database.collection("question_banks").findOne({_id:oid}, function(err,res){
                if (err) throw err;
                if (res != undefined)
                {
                    callback({JobName:jname,DepartmentName:departmentName,Questions:res.questions});
                }
            });
        });
    });
}

Cdata.DoesApplicationExist =function(department,jobid,name, callback)
{
    database.collection("applications").findOne({DepartmentId: department, JobPostingId: jobid, Submitter:name}, function(err,res){
        if (res)
        {
            callback(true);
        }else
        {
            callback(false);
        }
    });
}

Cdata.GetUserApplications =function(rblx, callback)
{
    database.collection("applications").find({Submitter: rblx}).toArray(function(err,res){
        //console.log(res);
        callback(res);

    });
}

Cdata.SubmitJob = function(body, department, jobid, callback)
{
    token = body.lgn_code;
    extra = JSON.parse(body.extra);
    //Get form questions
    Cdata.getJobPostingById(department, jobid, function(result){

        oid = new mongo.ObjectID(result.QuestionBankId);
            database.collection("question_banks").findOne({_id:oid}, function(err,res){
                QuestionBank = [];
                if (err) throw err;
                if (res != undefined)
                {
                    //console.log(res);
                    QuestionBank = res.questions;
                }
                
            for (i=0;i<QuestionBank.length;i++)
            {
                QuestionBank[i].Answer = body[`a_f_${i}`];
            }
            status = "SUBMITTED";
            postingtime = new Date().getTime();
            lastactiontime = new Date().getTime();
            submitter = "";
            profile.GetProfileFromToken(token, function(response){
                if (response != undefined)
                {
                    submitter = response.Username;
                }else
                {
                    callback("The application token is not valid.");
                    return;
                }
                Cdata.DoesApplicationExist(department,jobid,submitter, function(exist)
                {
             
                    if (exist)
                    {
                        callback("You cannot submit multiple applications to the same job posting.");
                    }else
                    {
                        
                        if (extra.special == "true")
                        {
                            if (extra.type=="ebbappeal")
                            {
                                userref = extra.ref;
                                refid = new mongo.ObjectID(userref);
                                database.collection("ebb").findOne({_id:refid}, (err,resx)=>{
                                    var options = { method: 'GET',
                                        url: 'https://api.roblox.com/users/get-by-username?username='+submitter,
                                    };
                                    request(options, function (error, response, body) {
                                        req = JSON.parse(body);
                                        
                                        if (resx.userid == req.Id)
                                        {
                                            QuestionBank.unshift({type:"text", question:"Blacklister", Answer:resx.submitter})
                                          
                                            QuestionBank.unshift({type:"text", question:"Tier", Answer:"Tier "+resx.tier})
                                            QuestionBank.unshift({type:"text", question:"Reason", Answer:resx.reason})
                                            
                                            QuestionBank.unshift({type:"text", question:"EBB Reference ID", Answer:resx._id})
                                            QuestionBank.unshift({type:"section", question:"APPEAL META DATA", Answer:undefined})
                                            database.collection("applications").insertOne({Questions:QuestionBank, DepartmentId: department, JobPostingId:jobid, Status:status, PostingTime:postingtime, LastActionTime: lastactiontime, Submitter:submitter, Comments:[]}, function(err,res){
                                                if (err)
                                                {
                                                    callback("The application was not able to be added to the database.");
                                                    return;
                                                }
                                                callback();
                                            });
                                        }else
                                        {
                                            callback("The blacklist must be against you in order to appeal it.");
                                            return;
                                        }
                                    });
                                });
                            }
                        }else
                        {
                            database.collection("applications").insertOne({Questions:QuestionBank, DepartmentId: department, JobPostingId:jobid, Status:status, PostingTime:postingtime, LastActionTime: lastactiontime, Submitter:submitter, Comments:[]}, function(err,res){
                                if (err)
                                {
                                    callback("The application was not able to be added to the database.");
                                    return;
                                }
                                callback();
                            });
                        }

                        
                    }
                })
                
            });
        });
    });
}

module.exports = Cdata;