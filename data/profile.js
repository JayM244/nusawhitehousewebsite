var Profile = {}
var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var database;
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
      database = db.db("whsite");
});


Profile.DoesProfileExist =function(robloxid, callback)
{
    database.collection("profiles").findOne({RobloxId:robloxid}, function(err,res){
        if (res != undefined)
        {
            callback(true);
        }else
        {
            callback(false);
        }
    })
}

Profile.CreateProfile=function(token,username,robloxid)
{
    tokens = [token];
    database.collection("profiles").insertOne({Username:username, RobloxId:robloxid, Tokens:tokens}, function(err,res){
        if (err) throw err;
    });
}

Profile.GetProfileFromToken=function(token, callback)
{
    database.collection("tokens").findOne({Token:token}, function(err,res){
        if (res != undefined)
        {
            rblxId = res.RobloxId;
            database.collection("profiles").findOne({RobloxId:rblxId}, function(err,res){
                if (res != undefined)
                {
                    callback(res);
                }
            });
        }
    })
}

Profile.GetTokens=function(robloxid,callback)
{
    database.collection("profiles").findOne({RobloxId:robloxid}, function(err,res){
        if (res != undefined)
        {
            callback(res.Tokens);
        }
    });
}

Profile.AddToken =function(token, username, robloxid)
{
    Profile.DoesProfileExist(robloxid,function(cb){
        if (cb)
        {
            Profile.GetTokens(robloxid, function(tokens)
            {
                tokens.push(token);
                database.collection("profiles").updateOne({RobloxId: robloxid}, {$set:{Tokens:tokens}}, function(err, res) {
                    if (err) throw err;
                });
            })
        }else{
            Profile.CreateProfile(token,username,robloxid);
        }
        database.collection("tokens").insertOne({Token: token, RobloxId: robloxid}, function(err,res)
        {
            if (err) throw err;
        });
    });
}

Profile.VerifyCode = function(code, callback)
{
    database.collection("verify").findOne({Code:code}, function(err,result){
        error=false;
        if (err) error=true;
        if (result == undefined)
        {
            callback();
            return;
        }
        Profile.AddToken(result.Token, result.Username, result.UserId)
        database.collection("verify").deleteOne({Code:code}, function(ero,res){
            if (ero) error=true;
        });
       
        callback(result.Token);
    });
}
module.exports = Profile;