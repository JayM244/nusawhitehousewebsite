Users = {}

Users.BuildUserCreator = function()
{
    return `<div class="card">
    <form action="" method="POST">
   
      <div class="card-body">
        <h4 class="card-title">New User</h4>
        <p class="card-description"> Create a new user</p>
        <div class="form-group">
          <label>Username</label>
          <input type="text" name="username" class="form-control form-control-lg" placeholder="ROBLOX Usernames ONLY" aria-label="Username"> </div>
          <div class="form-group">
              <label>Temp Password</label>
              <input  name="password" class="form-control form-control-lg" placeholder="They will change their password next login." aria-label="Username"> </div>
          
        
       
              <button type="submit" class="btn btn-inverse-primary btn-fw">Create User</button>
      </div>
         
    </form>
    </div>`;
}
Users.BuildUserList =function(elements)
{
    return `<div class="card">
    <div class="card-body">
      <h4 class="card-title">Active Users</h4>
      <div class="row">
        <div class="col-12">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                  <th>UserId</th>
                  <th></th>
                  <th>Username</th>
                <th>Department(s)</th>
                <th>Roles</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              ${elements}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>`
}
Users.BuildUserListResult = function(id, pfp, name, departments, roles)
{
    return `<tr>
    <td>${id}</td>
    <td><img class="img-xs rounded-circle" src="${pfp}" alt="Profile image"></td>
    <td>${name}</td>
  <td>${departments.toUpperCase()}</td>
  <td>${roles.toUpperCase()}</td>
  <td>
    <a href="./users/${id}/"><button class="btn btn-outline-primary">View</button></a>
  </td>
</tr>`
}
Users.BuildUserPage = function(all, userdeps, allperms, userperms)
{
    return `<div class="card">
    <div class="card-body">
    <form action="" method="POST">
        <h4 class="card-title">User Departments</h4>
        <div class="row">
            <div class="col-md-6 h-100">
              <div class="bg-light p-4">
                <h6 class="card-title">All Departments</h6>
                <div id="profile-list-left" class="py-2">
                    ${all}
                </div>
              </div>
            </div>
            <div class="col-md-6 h-100">
              <div class="bg-light p-4">
                <h6 class="card-title">User Departments</h6>
                <div id="profile-list-right" class="py-2">
                   ${userdeps}
                      </div>
                </div>
              </div>
            
            <button type="submit" name="action" value="departments" class="btn btn-inverse-primary btn-fw">Submit</button> <button type="submit" name="action" value="delete" class="btn btn-inverse-danger btn-fw">Delete User</button>
          </div>
          <br/>
          
          </form>
  </div>
</div>
<br/>
<div class="card">
    <div class="card-body">
    <form action="" method="POST">
        <h4 class="card-title">User Permissions</h4>
        <div class="row">
            <div class="col-md-6">
              <h6 class="card-title">All Permissions</h6>
              <div id="dragula-left" class="py-2">
                ${allperms}
               
              </div>
            </div>
            <div class="col-md-6">
              <h6 class="card-title">User Permissions</h6>
              <div id="dragula-right" class="py-2">
                 ${userperms}
              </div>
            </div>
          
          <br/>
          <button type="submit" name="action" value="perms" class="btn btn-inverse-primary btn-fw">Submit</button>
          </form>
  </div>
</div>`;
}

Users.BuildPermission =function(id, enabled)
{
  perms = {
    A:`<div class="card rounded border mb-2">
    <div class="card-body p-3">
      <div class="media">
        <i class="mdi mdi-code-tags icon-sm align-self-center mr-3"></i>
        <div class="media-body">
        <input style="display:none;" name="A" value="${enabled.toString()}">
          <h6 class="mb-1">Developer [A]</h6>
          <p class="mb-0 text-muted"> Can access all pages,and do anything (for testing reasons), however may not act in any official capacity of the White House.  </p>
        </div>
      </div>
    </div>
  </div>`,
  B:`<div class="card rounded border mb-2">
  <div class="card-body p-3">
    <div class="media">
      <i class="mdi mdi-newspaper icon-sm align-self-center mr-3"></i>
      <div class="media-body">
      <input style="display:none;" name="B" value="${enabled.toString()}">
        <h6 class="mb-1">Press [B]</h6>
        <p class="mb-0 text-muted">Can make publish official White House press briefings </p>
      </div>
    </div>
  </div>
</div>`,
C:`<div class="card rounded border mb-2">
<div class="card-body p-3">
  <div class="media">
    <i class="mdi mdi-gavel icon-sm align-self-center mr-3"></i>
    <div class="media-body">
    <input style="display:none;" name="C" value="${enabled.toString()}">
      <h6 class="mb-1">Policy Advisor [C]</h6>
      <p class="mb-0 text-muted"> Can publish policy articles </p>
    </div>
  </div>
</div>
</div>
`,
D:`<div class="card rounded border mb-2">
<div class="card-body p-3">
  <div class="media">
    <i class="mdi mdi-briefcase icon-sm align-self-center mr-3"></i>
    <div class="media-body">
    <input style="display:none;" name="D" value="${enabled.toString()}">
      <h6 class="mb-1">Career/Job Recruiter [D]</h6>
      <p class="mb-0 text-muted"> Can operate the job/recruitment system for their department. </p>
    </div>
  </div>
</div>
</div>`,
E:`  <div class="card rounded border mb-2">
<div class="card-body p-3">
  <div class="media">
    <i class="mdi mdi-hail icon-sm align-self-center mr-3"></i>
    <div class="media-body">
    <input style="display:none;" name="E" value="${enabled.toString()}">
      <h6 class="mb-1">Cabinet [E]</h6>
      <p class="mb-0 text-muted"> Manage Career/Job recruiters for their department. </p>
    </div>
  </div>
</div>
</div>`,
F: `       <div class="card rounded border mb-2">
<div class="card-body p-3">
  <div class="media">
    <i class="mdi mdi-file-question icon-sm align-self-center mr-3"></i>
    <div class="media-body">
    <input style="display:none;" name="F" value="${enabled.toString()}">
      <h6 class="mb-1">EBB Appeal Viewers [F]</h6>
      <p class="mb-0 text-muted"> Can look at EBB appeals, and make suggestions to the President, cannot add nor remove EBBs </p>
    </div>
  </div>
</div>
</div>
`,
G:`   <div class="card rounded border mb-2">
<div class="card-body p-3">
  <div class="media">
    <i class="mdi mdi-file-document-box-multiple icon-sm align-self-center mr-3"></i>
    <div class="media-body">
    <input style="display:none;" name="G" value="${enabled.toString()}">
      <h6 class="mb-1">WHCOS [G]</h6>
      <p class="mb-0 text-muted"> Can manage all users except EBB Appeal viewers.</p>
    </div>
  </div>
</div>
</div>`,
H: `    <div class="card rounded border mb-2">
<div class="card-body p-3">
  <div class="media">
    <i class="mdi mdi-account-tie icon-sm align-self-center mr-3"></i>
    <div class="media-body">
    <input style="display:none;" name="H" value="${enabled.toString()}">
      <h6 class="mb-1">POTUS/VPOTUS [H]</h6>
      <p class="mb-0 text-muted"> Mr President! </p>
    </div>
  </div>
</div>
</div>`
  }
  return perms[id];
}
Users.BuildDeparment = function(name, logo, id, enabled)
{
  return ` <div class="card rounded mb-2">
  <div class="card-body p-3">
    <div class="media">
      <img src="${logo}" alt="image" class="img-sm mr-3 rounded-circle">
      <div class="media-body">
        <h6 class="mb-1">${name}</h6>
        <input style="display:none;" name="${id}" value="${enabled.toString()}">
      </div>
    </div>
  </div>
</div>`;
}


module.exports=Users;