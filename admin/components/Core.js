Core = {}

Core.BuildNav =function(perms)
{
   
    if (perms.includes("A") || perms.includes("H") || perms.includes("G") || perms.includes("E"))
    {
      return `<li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#dashboard-dropdown" aria-expanded="false" aria-controls="dashboard-dropdown">
        <i class="menu-icon mdi mdi-briefcase-account"></i>
        <span class="menu-title">Careers</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="dashboard-dropdown">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="/admin/careers/posting/create">New Posting</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/careers/">View Applications</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="/admin/careers/postings/">View Postings</a>
        </li>
        </ul>
      </div>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="collapse" href="#dep-drop" aria-expanded="false" aria-controls="dep-drop">
          <i class="menu-icon mdi mdi-bank"></i>
          <span class="menu-title">Departments</span>
          <i class="menu-arrow"></i>
        </a>
        <div class="collapse" id="dep-drop">
          <ul class="nav flex-column sub-menu">
            <li class="nav-item">
              <a class="nav-link" href="/admin/departments/create">New Department</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="/admin/departments">View Departments</a>
            </li>
          </ul>
        </div>
      </li>
      <li class="nav-item">
          <a class="nav-link" data-toggle="collapse" href="#user-drop" aria-expanded="false" aria-controls="user-drop">
            <i class="menu-icon mdi mdi-account"></i>
            <span class="menu-title">Users and Groups</span>
            <i class="menu-arrow"></i>
          </a>
          <div class="collapse" id="user-drop">
            <ul class="nav flex-column sub-menu">
              <li class="nav-item">
                <a class="nav-link" href="/admin/users/create">Create User</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="/admin/users">Users</a>
              </li>
            </ul>
          </div>
        </li>
        <li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#doc-dropdown" aria-expanded="false" aria-controls="doc-dropdown">
        <i class="menu-icon mdi mdi-file-document"></i>
        <span class="menu-title">Documents</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="doc-dropdown">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="/admin/documents/create">Create Document</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/documents">View Documents</a>
          </li>
        
        </ul>
      </div>
      
    </li>
    <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#ebb-dropdown" aria-expanded="false" aria-controls="doc-dropdown">
      <i class="menu-icon mdi mdi-cancel"></i>
      <span class="menu-title">EBBs</span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="ebb-dropdown">
      <ul class="nav flex-column sub-menu">
        <li class="nav-item">
          <a class="nav-link" href="/admin/ebb/create">Issue EBB</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin/ebb">View EBBs</a>
        </li>
        
      </ul>
    </div>
    
  </li>
    
    `
    }
    if (perms.includes("D"))
    {
      return `<li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#dashboard-dropdown" aria-expanded="false" aria-controls="dashboard-dropdown">
        <i class="menu-icon mdi mdi-briefcase-account"></i>
        <span class="menu-title">Careers</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="dashboard-dropdown">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="/admin/careers/posting/create">New Posting</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/careers/">View Applications</a>
          </li>
          <li class="nav-item">
          <a class="nav-link" href="/admin/careers/postings/">View Postings</a>
        </li>
        </ul>
      </div>
    </li>`
    }
    if (perms.includes("B") || perms.includes("C")){
     return  `<li class="nav-item">
      <a class="nav-link" data-toggle="collapse" href="#doc-dropdown" aria-expanded="false" aria-controls="doc-dropdown">
        <i class="menu-icon mdi mdi-file-document"></i>
        <span class="menu-title">Documents</span>
        <i class="menu-arrow"></i>
      </a>
      <div class="collapse" id="doc-dropdown">
        <ul class="nav flex-column sub-menu">
          <li class="nav-item">
            <a class="nav-link" href="/admin/documents/create">Create Document</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/admin/documents">View Documents</a>
          </li>
        
        </ul>
      </div>
    </li>`
    }

    return "";
}


Core.BuildPage = function(title, username, pfp, body,nav, customJS)
{
    page = `<!DOCTYPE html>
    <html lang="en">
    
    <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>The White House - Admin</title>
      <!-- plugins:css -->
      <link rel="stylesheet" href="https://d3iv4xdecabd7n.cloudfront.net/materialdesignicons.css">
      <link rel="stylesheet" href="https://d3iv4xdecabd7n.cloudfront.net/feather.css">
      <link rel="stylesheet" href="https://d3iv4xdecabd7n.cloudfront.net/vendor.bundle.base.css">
      <link rel="stylesheet" href="https://d3iv4xdecabd7n.cloudfront.net/vendor.bundle.addons.css">
      <!-- endinject -->
      <!-- plugin css for this page -->
      <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.5.95/css/materialdesignicons.min.css">
      <!-- End plugin css for this page -->
      <!-- inject:css -->
      <link rel="stylesheet" href="https://nusawhitehouse.com/admin/css/shared/style.css">
      <!-- endinject -->
      <!-- Layout styles -->
      <link rel="stylesheet" href="https://nusawhitehouse.com/admin/css/demo_7/style.css">
      <!-- End Layout styles -->
      <link rel="shortcut icon" href="images/favicon.png" />
    </head>
    
    <body>
      <div class="container-scroller">
        <!-- partial:partials/_navbar.html -->
        <nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
          <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
            <a class="navbar-brand brand-logo" href="index.html">
              <img src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/5e/US-WhiteHouse-Logo.svg/1280px-US-WhiteHouse-Logo.svg.png" alt="logo" />
              <br/>
            </a>
            <a class="navbar-brand brand-logo-mini" href="index.html">
              <img src="https://res-2.cloudinary.com/crunchbase-production/image/upload/c_lpad,h_256,w_256,f_auto,q_auto:eco/v1499709566/fpmmw0lkalvvpbh1cbmv.png" alt="logo" />
            </a>
          </div>
          <div class="navbar-menu-wrapper d-flex align-items-center">
            <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
              <span class="mdi mdi-menu"></span>
            </button>
            <ul class="navbar-nav">
              
            </ul>
            <form action="#" class="form form-search ml-auto d-none d-md-flex">
              <div class="input-group">
                
              </div>
            </form>
            <ul class="navbar-nav">
              <li class="nav-item dropdown d-none d-xl-inline-flex">
                <a class="nav-link dropdown-toggle pl-4 d-flex align-items-center" id="UserDropdown" href="#" data-toggle="dropdown"
                  aria-expanded="false">
                  <div class="count-indicator d-inline-flex mr-3">
                    <img class="img-xs rounded-circle" src="${pfp}" alt="Profile image">
                    
                  </div>
                  <span class="profile-text font-weight-medium">${username}</span>
                </a>
                
              </li>
            </ul>
            <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
              <span class="icon-menu"></span>
            </button>
          </div>
        </nav>
        <!-- partial -->
        <div class="container-fluid page-body-wrapper">
          <!-- partial:partials/_sidebar.html -->
          <nav class="sidebar sidebar-offcanvas" id="sidebar">
                <br/>
              <ul class="nav">
                ${nav}
               
               </ul>
              
            </nav>
          <!-- partial -->
          <div class="main-panel">
            <div class="content-wrapper">
              <div class="content-header d-flex flex-column flex-md-row">
                <h4 class="mb-0">${title}</h4>
              </div>
                ${body}
                <!-- Put Content Here -->
              </div>
            </div>
            <!-- content-wrapper ends -->
            <!-- partial:partials/_footer.html -->
            <footer class="footer">
              <div class="container-fluid clearfix">
                
              </div>
            </footer>
            <!-- partial -->
          </div>
          <!-- main-panel ends -->
        </div>
        <!-- page-body-wrapper ends -->
      </div>
      <!-- container-scroller -->
      <!-- plugins:js -->
      <script src="https://d3iv4xdecabd7n.cloudfront.net/vendor.bundle.base.js"></script>
      <script src="https://d3iv4xdecabd7n.cloudfront.net/vendor.bundle.addons.js"></script>
      <!-- endinject -->
      <!-- Plugin js for this page-->
      <!-- End plugin js for this page-->
      <!-- inject:js -->
      <script src="https://nusawhitehouse.com/admin/js/shared/off-canvas.js"></script>
      <script src="https://nusawhitehouse.com/admin/js/shared/hoverable-collapse.js"></script>
      <script src="https://nusawhitehouse.com/admin/js/shared/misc.js"></script>
      
      <!-- endinject -->
        ${customJS}
    </body>
    
    </html>`
    return page;
}

module.exports = Core;