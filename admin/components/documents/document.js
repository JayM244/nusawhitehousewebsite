var doc = {}
doc.BuildCreator = function(categories)
{
    return `<div class="card">
    <form action="" method="POST" enctype="multipart/form-data">
      <div class="card-body">
        <h4 class="card-title">New Document Upload</h4>
        <p class="card-description"> Create a new document to be uploaded and featured. </p>
        <br/>
        <p class="card-description text-danger"> DO NOT UPLOAD CLASSIFIED DOCUMENTS USING THIS SERVICE. ALL DOCUMENTS ARE PUBLIC, UNLISTED OR NOT.</p>
        <div class="form-group">
          <label>Document Name</label>
          <input type="text" class="form-control form-control-lg" name="title" placeholder="Title" aria-label="Username"> </div>
          <div class="form-group">
              <label for="jdesc">Document Description</label> <textarea name="description"class="form-control" id="jdesc" rows="2"></textarea> </div>
          <div class="form-group">
              <label for="department">Document Type</label>
              <select name="category" class="form-control form-control-lg" id="department">
                ${categories}
              </select>
            </div>
            <div class="form-group">
            <label for="vis">Visibility</label>
            <select name="visible" class="form-control form-control-lg" id="vis">
            <option value=true>Listed</option>
            <option value=false>Unlisted</option>
            </select>
       
          </div>
            <div class="col-lg-4 grid-margin stretch-card">
   
                <div class="card-body">
                  <h4 class="card-title d-flex">PDF Document <small class="ml-auto align-self-end">
                    
                    </small>
                  </h4>
                  <input type="file" name="pdf"class="dropify" />
                </div>
    
            </div>
            <div class="col-lg-4 grid-margin stretch-card">
      
                <div class="card-body">
                  <h4 class="card-title d-flex">Cover Image <small class="ml-auto align-self-end">
                    
                    </small>
                  </h4>
                  <input type="file" name="cover_image"class="dropify" />
     
              </div>
            </div>
              <button type="submit" class="btn btn-inverse-primary btn-fw">Create Document</button>
      </div>
    </form>
    </div>`
}
doc.BuildCategory = function(category_name, category_id)
{
    return `<option value="${category_id}">${category_name}</option>`
}

doc.BuildList = function(listings)
{
  return `<div class="card">
  <div class="card-body">
    <h4 class="card-title">All Documents</h4>
  


    <div class="row">
      <div class="col-12">
        <table id="order-listing" class="table">
          <thead>
            <tr>
             
              <th>Id</th>
              <th>Name</th>
              <th>Submitter</th>
              <th>Type</th>
              <th>Action</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            ${listings}
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>`
}
doc.BuildListResult = function(id, name, submitter, type)
{
  return `<tr>
               
  <td>${id}</td>
  <td>${name}</td>
  <td>${submitter}</td>
  <td>${type}</td>


  <td>
    <form action="" method="POST">
    <button name="document" value="${id}" class="btn btn-outline-danger">Delete</button>   </form>
  </td>
  <td>
    <form action="/documents/${id}" method="GET">
    <button class="btn btn-outline-primary">View</button>   </form>
  </td>
</tr>`;
}

module.exports = doc;