Posting = {}
Posting.BuildPostingCreator = function(department, banks)
{
    return `<div class="card">
    <form action="" method="POST">
      <div class="card-body">
        <h4 class="card-title">New Job Posting</h4>
        <p class="card-description"> Create a new job posting on the public job application page. </p>
        <div class="form-group">
          <label>Job Title</label>
          <input type="text" class="form-control form-control-lg" name="title" placeholder="Title" aria-label="Username"> </div>
          <div class="form-group">
              <label for="jdesc">Job Description</label> <textarea name="description" class="form-control" id="jdesc" rows="2"></textarea> </div>
          <div class="form-group">
              <label for="department">Department</label>
              <select name="department" class="form-control form-control-lg" id="department">
                ${department}
              
              </select>
            </div>
            <div class="form-group">
                <label for="qbank">Question Bank</label>
                <select name="questions" class="form-control form-control-lg" id="qbank">
                  ${banks}
                  
                </select>
              </div>
              <button type="submit" class="btn btn-inverse-primary btn-fw">Create Posting</button>
      </div>
    </form>
    </div>`;
}
Posting.BuildQuestionOption = function(name, id)
{
    return `<option value="${id}">${name} (${id})</option>`
}
Posting.BuildDeparmentOption = function(name, id)
{
    return `<option value="${id}">${name}</option>`
}
Posting.BuildIndividualPosting = function(username,departmentName, postiion, submitId, qa, comments)
{
    return `<div class="card">
    <div class="card-body">
        <h4 class="card-title">Application Metadata</h4>
        <b>Username: </b>${username}<br/>
        <b>Position: </b>${postiion}<br/>
        <b>Department: </b>${departmentName}<br/>
        <b>SubmissionId: </b>${submitId}
  </div>
</div>
<br/>
  <div class="card">
      <div class="card-body">
        <h4 class="card-title">Application Viewer</h4>

        <table class="table table-hover table-break" >
          <thead>
            <tr>
              <th>Question</th>
              <th>Answer</th>
            
            </tr>
          </thead>
          <tbody>
            ${qa}
          </tbody>
        </table>
        <form action="" method="POST">
        <button type="submit"name="action"value="interview" class="btn btn-inverse-primary btn-fw">Request Interview</button>  <button type="submit"name="action"value="declined" class="btn btn-inverse-danger btn-fw">Decline</button> <button type="submit"name="action"value="accepted" class="btn btn-inverse-success btn-fw">Accept</button> <button type="submit"name="action"value="purge" class="btn btn-danger btn-fw">! PURGE !</button></form>
      </div>
     
    </div>
    <br/>
    <div class="card">
      <div class="card-body">
        <div class="d-flex justify-content-between">
          <h4 class="card-title">Comments</h4>
        
        </div>
        <p class="card-description">What people are saying</p>
        ${comments}
        
        <br/>
       <div class="row">
       <form action="/admin/careers/comment/${submitId}" method="POST">
         <div class="col-6">
          <div class="form-group">
            <label for="comment">Comment something meaningful</label> <textarea name="comment" class="form-control" id="comment" rows="2"></textarea> </div>
         </div>
         <div class="col-3">
           <br/><br/>
          <button type="submit" name="action" value="comment" class="btn btn-inverse-primary btn-fw">Comment</button>
         </div> 
         </form>
       </div>
      </div>
    </div>`;
}
Posting.BuildQuestionAnswer =function(question, answer)
{
    return  `<tr>
    <td>${question}</td>
    <td>${answer}</td>
  </tr>`
}
Posting.BuildComment = function(username, pfp, comment)
{
    return `
    <div class="list d-flex align-items-center border-bottom py-3">
                  <img class="img-sm rounded-circle" src="${pfp}" alt="">
                  <div class="wrapper w-100 ml-3">
                    <p class="mb-0">
                      <b>${username} </b>posted</p>
                    <div class="d-flex justify-content-between align-items-center">
                      <div class="d-flex align-items-center">
                        <i class="mdi mdi-message text-muted mr-1"></i>
                        <p class="mb-0">${comment}</p>
                      </div>
                   
                    </div>
                  </div>
                </div>`
}

Posting.BuildViewPage = function(departments, results)
{
    return ` <div class="card">
    <div class="card-body">
      <h4 class="card-title">Application Data</h4>
      <div class="row">
      
        <div class="col-md-3">
            <div class="form-group">
                <label for="deps">Select Department</label>
                <select class="form-control form-control-lg" name="department" id="deps">
                  ${departments}
                
                </select>
              </div>
            </div>
            <div class="col-md-6">
              <br/>
                <button type="button" onclick="document.location='/admin/careers/' + document.getElementById('deps').value" class="btn btn-lg btn-outline-primary">View</button>
            </div>
            <hr/>
       
      </div>

      <br/>
      <div class="row">
        <div class="col-12">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>Submitter</th>
                <th>Position</th>
                <th>Department</th>
                <th>Status</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              ${results}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>`
}

Posting.BuildViewResult = function(id, submitter, position, department, submissiondate, lastactiondate, status)
{
    return ` <tr>
    <td>${submitter.toUpperCase()}</td>
    <td>${position.toUpperCase()}</td>
    <td>${department.toUpperCase()}</td>
    <td>
        ${status}
    </td>
    <td>
      <a href="./application/${id}/"><button class="btn btn-outline-primary">View</button></a>
    </td>
  </tr>`
}
Posting.BuildViewStatus = function(status)
{
    reso = {
         ACCEPTED: `<label class="badge badge-success">ACCEPTED</label>`,
    SUBMITTED: `<label class="badge badge-primary">SUBMITTED</label>`,
    OPENED: `<label class="badge badge-info">OPENED</label>`,
    INTERVIEW: `<label class="badge badge-warning">INTERVIEW PENDING</label>`,
    DECLINED:`<label class="badge badge-danger">DECLINED</label>`};
    return reso[status];
}
Posting.BuildManager= function(postings, departments)
{
    return `<div class="card">
    <div class="card-body">
      <h4 class="card-title">Active Job Postings</h4>
      <div class="row">
      
      <div class="col-md-3">
          <div class="form-group">
              <label for="deps">Select Department</label>
              <select class="form-control form-control-lg" name="department" id="deps">
                ${departments}
              
              </select>
            </div>
          </div>
          <div class="col-md-6">
            <br/>
              <button type="button" onclick="document.location='/admin/careers/postings/' + document.getElementById('deps').value" class="btn btn-lg btn-outline-primary">View</button>
          </div>
          <hr/>
     
    </div>


      <br/>
      <div class="row">
        <div class="col-12">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>ID</th>
                <th>Department</th>
                <th>Name</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              ${postings}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>`
}
Posting.BuildManagerResult =function(id, department, name)
{
    return `<tr>
    <td>${id}</td>
    <td>${department}</td>
    <td>${name}</td>


    <td>
    <form action="./${id}/delete" method="POST">
      <button type="submit" name="job" value="${id}" class="btn btn-outline-danger">Delete</button></form>
      <form action="./${id}/vanish" method="POST">
      <button type="submit" name="job" value="${id}" class="btn btn-outline-warning">Deactivate</button></form>
    </td>

    
  </tr>`
}
function insertString(a, b, at)
{
    var position = at; 

    if (position !== -1)
    {
        return a.substr(0, position) + b + a.substr(position);    
    }  

}
Posting.Sanitize =function(styff)
{
    for(i=65;i<styff.length;i+=65)
    {
        styff = insertString(styff, "<br/>", i);
    }
    return styff;
}

module.exports = Posting;