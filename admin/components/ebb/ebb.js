ebb = {}

ebb.CreateIssueForm = () => {
    return `<div class="card">
    <form action="" method="POST">
      <div class="card-body">
        <h4 class="card-title">Issue new EBB</h4>
        <p class="card-description"> Issue a new Executive Branch Blacklist </p>
        <div class="form-group">
          <label>ROBLOX Username</label>
          <input type="text" class="form-control form-control-lg" name="username" placeholder="Username" aria-label="Username"> </div>
          <div class="form-group">
              <label for="jdesc">Reason</label> <textarea name="reason" class="form-control" id="jdesc" rows="2"></textarea> </div>
          <div class="form-group">
              <label for="department">Department</label>
              <select name="tier" class="form-control form-control-lg" id="tier">
                <option value="1">Tier 1</option> 
                <option value="2">Tier 2</option> 
                <option value="3">Tier 3</option> 
              </select>
            </div>
       
              <button type="submit" class="btn btn-inverse-primary btn-fw">Blacklist</button>
      </div>
    </form>
    </div>`
}
ebb.BuildList = function(listings)
{
  return `<div class="card">
  <div class="card-body">
    <h4 class="card-title">All Blacklists</h4>
  


    <div class="row">
      <div class="col-12">
        <table id="order-listing" class="table">
          <thead>
            <tr>
             
              <th>Reference Id</th>
              <th>Name</th>
              <th>UserId</th>
              <th>Tier</th>
              <th>SubmissionTime</th>
              <th>Submitter</th>
              <th>Action</th>
              
            </tr>
          </thead>
          <tbody>
            ${listings}
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>`
}
ebb.BuildListResult = function(id, name, UserId, tier, time, submitter)
{
  return `<tr>
               
  <td>${id}</td>
  <td>${name}</td>
  <td>${UserId}</td>
  <td>${tier}</td>
  <td>${time}</td>
  <td>${submitter}</td>



  <td>
    <form action="" method="POST">
    <button name="ebb" value="${id}" class="btn btn-outline-danger">Delete</button>   </form>
  </td>

</tr>`;
}
module.exports = ebb;