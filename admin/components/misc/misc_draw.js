misc = {}

misc.BuildLoginPage = function(){
    return `<!DOCTYPE html>
    <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>The White House - Login</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="/admin/vendors/iconfonts/puse-icons-feather/feather.css">
        <link rel="stylesheet" href="/admin/vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="/admin/vendors/css/vendor.bundle.addons.css">
        <!-- endinject -->
        <!-- plugin css for this page -->
        <!-- End plugin css for this page -->
        <!-- inject:css -->
        <link rel="stylesheet" href="/admin/css/shared/style.css">
        <!-- endinject -->
        <link rel="shortcut icon" href="/admin/images/favicon.png" />
      </head>
      <body>
        <div class="container-scroller">
          <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center auth auth-bg-1 theme-one">
              <div class="row w-100">
                <div class="col-lg-4 mx-auto">
                  <div class="auto-form-wrapper">
                    <form action="/admin/login" method="POST">
                      <div class="form-group">
                        <label class="label">Username</label>
                        <div class="input-group">
                          <input type="text" class="form-control" name="username"  placeholder="Username">
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <i class="mdi mdi-check-circle-outline"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="label">Password</label>
                        <div class="input-group">
                          <input type="password" class="form-control" name="password" placeholder="*********">
                          <div class="input-group-append">
                            <span class="input-group-text">
                              <i class="mdi mdi-check-circle-outline"></i>
                            </span>
                          </div>
                        </div>
                      </div>
                      <div class="form-group">
                        <button class="btn btn-primary submit-btn btn-block">Login</button>
                      </div>
                    </form>
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- content-wrapper ends -->
          </div>
          <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="vendors/js/vendor.bundle.base.js"></script>
        <script src="vendors/js/vendor.bundle.addons.js"></script>
        <!-- endinject -->
        <!-- inject:js -->
        <script src="js/shared/off-canvas.js"></script>
        <script src="js/shared/hoverable-collapse.js"></script>
        <script src="js/shared/misc.js"></script>
        <script src="js/shared/settings.js"></script>
        <script src="js/shared/todolist.js"></script>
        <!-- endinject -->
      </body>
    </html>`;
}

misc.Build401Page = function()
{
    return `<!DOCTYPE html>
    <html lang="en">
      <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>The White House - 401</title>
        <!-- plugins:css -->
        <link rel="stylesheet" href="/admin/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
        <link rel="stylesheet" href="/admin/vendors/iconfonts/puse-icons-feather/feather.css">
        <link rel="stylesheet" href="/admin/vendors/css/vendor.bundle.base.css">
        <link rel="stylesheet" href="/admin/vendors/css/vendor.bundle.addons.css">
        <!-- endinject -->
        <!-- inject:css -->
        <link rel="stylesheet" href="/admin/css/shared/style.css">
        <!-- endinject -->
        <link rel="shortcut icon" href="images/favicon.png" />
      </head>
      <body>
        <div class="container-scroller">
          <div class="container-fluid page-body-wrapper full-page-wrapper">
            <div class="content-wrapper d-flex align-items-center text-center error-page bg-danger">
              <div class="row flex-grow">
                <div class="col-lg-7 mx-auto text-white">
                  <div class="row align-items-center d-flex flex-row">
                    <div class="col-lg-6 text-lg-right pr-lg-4">
                      <h1 class="display-1 mb-0">401</h1>
                    </div>
                    <div class="col-lg-6 error-page-divider text-lg-left pl-lg-4">
                      <h2>ZOINKS!</h2>
                      <h3 class="font-weight-light">You permissions dont allow you to be here.</h3>
                    </div>
                  </div>
                  <div class="row mt-5">
                    <div class="col-12 text-center mt-xl-2">
                      <a class="text-white font-weight-medium" href="/">Back to home</a>
                    </div>
                  </div>
                  <div class="row mt-5">
                    <div class="col-12 mt-xl-2">
                      <p class="text-white font-weight-medium text-center">Copyright &copy; 2018 All rights reserved.</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- content-wrapper ends -->
          </div>
          <!-- page-body-wrapper ends -->
        </div>
        <!-- container-scroller -->
        <!-- plugins:js -->
        <script src="/admin/vendors/js/vendor.bundle.base.js"></script>
        <script src="/admin/vendors/js/vendor.bundle.addons.js"></script>
        <!-- endinject -->
        <!-- inject:js -->
        <script src="/admin/js/shared/off-canvas.js"></script>
        <script src="/admin/js/shared/hoverable-collapse.js"></script>
        <script src="/admin/js/shared/misc.js"></script>
        <script src="/admin/js/shared/settings.js"></script>
        <script src="/admin/js/shared/todolist.js"></script>
        <!-- endinject -->
      </body>
    </html>`
}

module.exports = misc;