Department = {};

Department.BuildDepartmentCreator =function()
{
return ` <div class="card">
<form action="" method="POST">
  <div class="card-body">
    <h4 class="card-title">New Department</h4>
    <p class="card-description"> Create a new department to add users to, and manage job postings for</p>
    <div class="form-group">
      <label>Department Name</label>
      <input type="text"name="dname" class="form-control form-control-lg" placeholder="Name" aria-label="Username"> </div>
      <div class="form-group">
          <label>Department Logo</label>
          <input type="text"name="dlogo" class="form-control form-control-lg" placeholder="Logo must be transparent and end with '.png'" aria-label="Username"> </div>
      <div class="form-group">
          <label for="jdesc">Department Description</label> <textarea name="ddesc" class="form-control" id="jdesc" rows="2"></textarea> </div>
    
   
          <button type="submit" class="btn btn-inverse-primary btn-fw">Create Department</button>
  </div>
</form>
</div>`
}
Department.BuildDepartmentViewer = function(body)
{
    return `<div class="card">
    <div class="card-body">
      <h4 class="card-title">Active Departments</h4>
      <div class="row">
        <div class="col-12">
          <table id="order-listing" class="table">
            <thead>
              <tr>
                <th>Department</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            ${body}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>`
}
Department.BuildDepartmentResult = function(name, id)
{
    return `<tr>       
    <td>${name.toUpperCase()}</td>
    <td>
      <form action="" method="POST">
      <button name="department" value="${id}" class="btn btn-outline-danger">Delete</button>   </form>
    </td>
  </tr>`
}
module.exports = Department;