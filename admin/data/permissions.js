var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var database;
var mongo = require('mongodb');
var XMLHttpRequest = require('xhr2');
var passwordhash = require('password-hash');
const request = require('request');
var Misc = require('../components/misc/misc_draw');


MongoClient.connect(url, function(err, db) {
    if (err) throw err;
      database = db.db("whsite");
});

Perms = {}
Perms.GetUserPermissions = function(user, callback)
{
    if (mongo.ObjectID.isValid(user))
    {
        oid = new mongo.ObjectID(user);
    }else
    {
        oid = "";
    }
    database.collection("users").findOne({_id:oid}, (err,resu)=>{
        if (resu)
        {
            callback(resu.permissions);
        }
        
    });
}
Perms.GetUserDepartments = function(user, callback)
{
    if (mongo.ObjectID.isValid(user))
    {
        oid = new mongo.ObjectID(user);
    }else
    {
        oid = "";
    }
    database.collection("users").findOne({_id:oid}, (err,resu)=>{
        callback(resu.departments);
    });
}
Perms.ValidUser = function(auth_token, callback)
{
    database.collection("admin_tokens").findOne({token:auth_token}, (err,res)=>{
        if (res) callback(res.user);
        else callback(false);
    })
}


Perms.AuthAPI = (token, callback) =>
{
    if (token)
    {
        Perms.ValidUser(token, (v)=>{
            if (v)
            {
                Perms.GetUserPermissions(v, (up)=>{
                    //console.log(up);
                    if (up.includes("A") || up.includes("H") || up.includes("G")|| up.includes("E")|| up.includes("D") || perms=="Z")
                    {
                        
                        callback(true);
                    }else{
                        
                        
                        callback(false)
                    }
                });
            }else
            {
                callback(false)
            }
            
        });
    }
}

Perms.PerformUserAuth =function(req,res,perms, callback)
{
    if (req.cookies)
    {
  
        if (req.cookies.admin)
        {
            //console.log("Jello?");
            //Authenticate that token
            Perms.ValidUser(req.cookies.admin, (v)=>{
                //See if the user is allowed to be here.
                Perms.GetUserPermissions(v, (up)=>{
                    //console.log(up);
                    if (up.includes("A") || up.includes("H") || up.includes("G")|| up.includes("E")|| up.includes(perms) || perms=="Z")
                    {
                        
                        callback(v);
                    }else{
                        
                        res.send(Misc.Build401Page());
                        callback()
                    }
                });
            });
            
        }else
        {
            res.send(Misc.BuildLoginPage());
            callback();
        }
    }else
    {
        res.send(Misc.BuildLoginPage());
        callback();
    }
    
}





Perms.DeleteUser = function(user)
{
    if (mongo.ObjectID.isValid(user))
    {
        oid = new mongo.ObjectID(user);
    }else
    {
        oid = "";
    }
    database.collection("users").deleteOne({_id:oid}, (err)=>{

    });
}

Perms.GetUserData = function(user, callback)
{
    if (mongo.ObjectID.isValid(user))
    {
        oid = new mongo.ObjectID(user);
    }else
    {
        oid = "";
    }
    database.collection("users").findOne({_id:oid}, (err,resu)=>{
        callback(resu);
    });
}
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
       result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
 }
Perms.Login = function(username, password, callback)
{
    database.collection("users").findOne({username: username}, (err,res)=>{
        if(res)
        {
            if (passwordhash.verify(password, res.password))
            {
                token = makeid(48);
                database.collection("admin_tokens").insertOne({token:token, user:res._id}, (err)=>{
                    callback(token);
                });
            }else
            {
                callback();
            }
        }else
        {
            callback();
        }
        
    });
}
Perms.GetUsers = function(callback)
{
    database.collection("users").find({}).toArray((err,res)=>{
        callback(res);
    });
}
Perms.CreateUser =function(username,password, callback)
{
    request('https://api.roblox.com/users/get-by-username?username='+username, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        //console.log(body.Id);
        resp = body;
        id = resp.Id;
        request(`https://thumbnails.roblox.com/v1/users/avatar-headshot?userIds=${id}&size=48x48&format=Png`, { json: true }, (erra, resa, bodya) => {
            if (erra) { return console.log(erra); }
            respa= bodya;
            picture = respa.data[0].imageUrl;
            
            database.collection("users").insertOne({username:username, password:passwordhash.generate(password), departments:[], permissions:"",pfp:picture}, (e,r)=>{
                callback(r._id);
            })
        });
    });
}
Perms.GetUserById = function(id, callback)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("users").findOne({_id: oid}, (err,res)=>{
        callback(res);
    });
}
Perms.UpdatePerms = function(id, newPerms)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("users").updateOne({_id:oid}, {$set:{permissions:newPerms}}, (err)=>{

    });   
}
Perms.HasDepartment = async function(id, departmentId)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    res = database.collection("users").findOne({_id:oid});
    if (res)
    {
        for(i=0;i<res.departments.length;i++)
        {
            if (res.departments[i] == departmentId)
            {
                return true;
            }
        }
        return false;
    }
}
module.exports = Perms;