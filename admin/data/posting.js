var MongoClient = require('mongodb').MongoClient;
var mongo = require('mongodb');
var url = "mongodb://localhost:27017/";
var database;
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
      database = db.db("whsite");
});
  
var Posting = {}
Posting.GetQuestionBanks = function(callback)
{
    database.collection("question_banks").find({}).toArray(function(err, res){
        callback(res);
    });
}
Posting.GetApplicationsFromDepartment = async function(department, callback)
{
    database.collection("applications").find({DepartmentId: department}).toArray(async function(err,res){
        for(i=0;i<res.length;i++)
        {
            oid = new mongo.ObjectID(res[i].JobPostingId);
            oid_ = new mongo.ObjectID(res[i].DepartmentId);
            posting = await database.collection("postings").findOne({_id:oid});
            department = await database.collection("departments").findOne({_id:oid_});
            res[i].JobName = posting.JobName;
            res[i].DepartmentName = department.DepartmentName;
        }
        callback(res);
    });
}
Posting.GetUserApplication = function(id, callback){
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("applications").findOne({_id: oid}, (err,res)=>{
        callback(res);
    });
}
Posting.CreatePosting = function(bank, name, department, description){
    database.collection("postings").insertOne({QuestionBankId: bank, JobName:name, DepartmentId: department, JobDescription:description}, (err)=>{
        
    });
}
Posting.DeletePosting = function(id)
{
    oid = new mongo.ObjectID(id);
    database.collection("applications").deleteMany({JobPostingId:oid}, (err)=>{
    
    })
    database.collection("postings").deleteOne({_id:oid}, (err)=>{

    })
}
Posting.Vanish=function(id)
{
    oid = new mongo.ObjectID(id);
    database.collection("postings").updateOne({_id:oid}, {$set:{vanished:true}}, (e)=>{

    });
}

Posting.SetApplicationStatus =function(id, status)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("applications").updateOne({_id:oid}, {$set:{Status:status, LastActionTime:new Date().getTime()}}, (err)=>{
        //console.log(status);
        //console.log(id);
    });
}

Posting.DeleteApplication = function(id)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("applications").deleteOne({_id:oid}, (err)=>{});
}

Posting.CreateComment = function(id, comment, submitter_un, pfp)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("applications").findOne({_id:oid}, (err,res)=>{
        //console.log(res);
        comments = res.Comments;
        comments.push({Username:submitter_un, pfp: pfp, text:comment});
        database.collection("applications").updateOne({_id:oid}, {$set:{Comments:comments}}, (r)=>{
            //console.log(r);
        });
    });
}
module.exports = Posting;