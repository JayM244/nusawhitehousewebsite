var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var database;
var mongo = require('mongodb');
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
      database = db.db("whsite");
});

var dmanage = {};

dmanage.CreateDepartment =function(name,logo,desc)
{
    database.collection("departments").insertOne({Description: desc, DepartmentName: name, DepartmentImage: logo}, (err)=>{

    });
}
dmanage.DeleteDepartment = function(id)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    
    database.collection("departments").deleteOne({_id:oid}, (err)=>{

    });
}
dmanage.GetDepartments = function(callback)
{
    database.collection("departments").find({}).toArray((err,res)=>{
        callback(res);
    });
}
dmanage.UpdateUserDepartments = function(id, newDeps){
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("users").updateOne({_id:oid}, {$set:{departments:newDeps}}, (err)=>{

    });
}
module.exports = dmanage;