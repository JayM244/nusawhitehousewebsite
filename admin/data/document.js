var MongoClient = require('mongodb').MongoClient;
var url = "mongodb://localhost:27017/";
var database;
var mongo = require('mongodb');
var perms = require('./permissions');
var moment = require('moment');
MongoClient.connect(url, function(err, db) {
    if (err) throw err;
      database = db.db("whsite");
});

doc = {}

doc.getCategories =function(user, callback)
{
    perms.GetUserPermissions(user, (perms)=>{
     
        database.collection("categories").find({Permissions: perms}).toArray((err,res)=>{
       
            callback(res);
        });
     });
}

doc.createDocument = function(name, user, desc, image, category, link, vis)
{
    date = moment().format("MM/DD/YYYY");
    database.collection("publishings").insertOne({Name:name, Description:desc, Image:image, Submitter: user, Category:category, Link:link, Date:date, List:vis}, (err)=>{})
}

doc.getDocuments = function(callback)
{
    database.collection("publishings").find({}).toArray((err,res)=>{
        callback(res);
    });
}
doc.deleteDocument =function(id)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    database.collection("publishings").deleteOne({_id: oid}, (err)=>{

    });
}
doc.getCategory = async function(id)
{
    if (mongo.ObjectID.isValid(id))
    {
        oid = new mongo.ObjectID(id);
    }else
    {
        oid = "";
    }
    category = await database.collection("categories").findOne({_id: oid});
    return category;
}


module.exports = doc;