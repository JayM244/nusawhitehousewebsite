var express = require('express');
var router = express.Router();
var careers = require('./routers/careers');
var departments = require('./routers/departments');
var misc = require('./components/misc/misc_draw');
var bodyParser = require('body-parser');
var perms = require('./data/permissions');
var core = require('./components/Core');
var users = require('./routers/users');
var cookieParser = require('cookie-parser');
var documents = require('./routers/documents');
var file = require('express-fileupload');
var ebb = require('./routers/ebb');

router.use(cookieParser());
urlencodedParser = bodyParser.urlencoded({ extended: false });

router.use(express.static('admin/public'));

router.use('/careers', careers);

router.use('/ebb', ebb)

router.use('/departments', departments);

router.get('/login', (req,res)=>{
    res.send(misc.BuildLoginPage());
});

router.use('/users', users)

router.use('/documents', documents);

router.get('/api/key', (req,res) =>{
    atoken = req.cookies.admin;
    if (atoken)
    {
        res.send("Do not give this token out to anyone." + `<br/> <b>${atoken}</b>`);
    }else
    {
        res.redirect("/login");
    }
    
});


router.post('/login',urlencodedParser, (req,res)=>{
    username = req.body.username;
    password = req.body.password;
    perms.Login(username, password, (result) =>{
        if (!result){
            res.send(misc.BuildLoginPage());
            return;
        }
        res.cookie("admin", result);
        res.redirect("/admin/");
        res.send();
    });
});

router.get('/', (req,res) =>{
    //console.log(req.cookies);
    perms.PerformUserAuth(req, res,"Z", (userId)=>{
        if (!userId) return;
        perms.GetUserData(userId, (data)=>{
            sideNav = Core.BuildNav(data.permissions);
            page = Core.BuildPage("Dashboard", data.username, data.pfp, "", sideNav,"");
            res.send(page);
        });
    });
});


module.exports = router;