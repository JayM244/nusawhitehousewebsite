var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var permsManager = require('../data/permissions');
var Departments = require('../components/departments/department');
var Core = require('../components/Core');
var jobDataManager = require('../../data/careers');
var departManager = require('../data/department');
var cookieParser = require('cookie-parser');
var ebbdata = require('../data/ebb');
var moment = require('moment');
urlencodedParser = bodyParser.urlencoded({ extended: false });

var ebb = require('../components/ebb/ebb');

router.get('/', (req,res)=>{
    permsManager.PerformUserAuth(req, res,"H", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, async (data)=>{
            
            ebbdata.GetBlacklists((blacklists)=>{
                
                blElements = ""
                for (i=0;i<blacklists.length;i++)
                {
                    cur = blacklists[i];
                    submitTime = moment(cur.submitTime);
                    submitTime.format('MMM Do H:m A z');
                    blElements += ebb.BuildListResult(cur._id, cur.username, cur.userid, cur.tier, submitTime, cur.submitter);
                }


                page = ebb.BuildList(blElements);
                sideNav = Core.BuildNav(data.permissions);
                page = Core.BuildPage("EBB", data.username, data.pfp, page, sideNav,"");
                res.send(page);
            });

           
        });
    });
});


router.get('/create', (req,res)=>{
    permsManager.PerformUserAuth(req, res,"H", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, async (data)=>{
        
            page = ebb.CreateIssueForm();
            sideNav = Core.BuildNav(data.permissions);
            page = Core.BuildPage("EBB", data.username, data.pfp, page, sideNav,"");
            res.send(page);
        });
    });
});


router.post('/create', urlencodedParser, (req,res)=>{
    username = req.body.username;
    reason = req.body.reason;
    tier = req.body.tier;
    permsManager.PerformUserAuth(req, res,"H", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, async (data)=>{
            ebbdata.IssueBlacklist(username, tier, reason, data.username, (id)=>{
                res.redirect("/admin/ebb");
            });
        });
       
    });
});

router.post('/', urlencodedParser, (req,res)=>{
    val = req.body.ebb;
    permsManager.PerformUserAuth(req, res,"H", (userId)=>{
        if (!userId) return;
        ebbdata.RemoveBlacklist(val, ()=>{
            res.redirect("/admin/ebb");
        });
       
    });
});

module.exports = router;