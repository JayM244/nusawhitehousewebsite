var express = require('express');
var router = express.Router();
var permsManager = require('../data/permissions');
var Core = require('../components/Core');
var Misc = require('../components/misc/misc_draw');
var Posting = require('../components/postings/posting');
var jobDataManager = require('../../data/careers');
var postingManager = require('../data/posting');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var moment = require('moment');

router.use(cookieParser());
urlencodedParser = bodyParser.urlencoded({ extended: false });
router.get('/', (req, res)=>{
    //User has a valid authentication token
    permsManager.PerformUserAuth(req,res,"D", (id) =>{
        if (!id) return;
        permsManager.GetUserDepartments(id, (departments)=>{
            res.redirect("./" + departments[0]);
            res.send();
        });
    });
});

router.get('/posting/create', (req,res)=>{
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, async (data)=>{
            departmentOptions = "";
            for(i=0;i<data.departments.length;i++)
            {
                dep = await jobDataManager.Async.getDepartmentById(data.departments[i])
                departmentOptions += Posting.BuildDeparmentOption(dep.DepartmentName, data.departments[i]);
            }
            questionBanks = ""
            postingManager.GetQuestionBanks((banks)=>{
                for (i=0;i<banks.length;i++)
                {
                    questionBanks+= Posting.BuildQuestionOption(banks[i].name, banks[i]._id);
                }
                page = Posting.BuildPostingCreator(departmentOptions, questionBanks);
                sideNav = Core.BuildNav(data.permissions);
                page = Core.BuildPage("Create Posting", data.username, data.pfp, page, sideNav,"");
                res.send(page);
            });
            
        });
    });
});
router.get('/postings/:departmentId', (req,res)=>{
    depart = req.params.departmentId;
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, async (data)=>{
            //Get all postings
            pdata = "";
            departmentOptions = "";
            for(i=0;i<data.departments.length;i++)
            {
                dep = await jobDataManager.Async.getDepartmentById(data.departments[i])
                departmentOptions += Posting.BuildDeparmentOption(dep.DepartmentName, data.departments[i]);
            }
            dname = await jobDataManager.Async.getDepartmentById(depart);
            jobDataManager.getJobPostings(depart, (postings) =>{
                for(i=0;i<postings.length;i++)
                {
                    current = postings[i];
                    if (!current.vanished)
                    {
                        pdata += Posting.BuildManagerResult(current._id, dname.DepartmentName, current.JobName);
                    }
                    //console.log(dname);
                   
                }
                pdata = Posting.BuildManager(pdata, departmentOptions);
                sideNav = Core.BuildNav(data.permissions);
                pdata = Core.BuildPage("Job Postings", data.username, data.pfp, pdata, sideNav,"");
                res.send(page);
            });
        });
    });
});
router.get('/postings/', (req,res)=>{
    permsManager.PerformUserAuth(req,res,"D", (id) =>{
        if (!id) return;
        permsManager.GetUserDepartments(id, (departments)=>{
            res.redirect("./" + departments[0]);
            res.send();
        });
    });
});

router.get('/:departmentId', (req,res)=>{
    depart = req.params.departmentId;
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, async (data)=>{
            //First load the page
            departmentOptions = "";
            //console.log(data)
            for(i=0;i<data.departments.length;i++)
            {
                //console.log(data);
                dep = await jobDataManager.Async.getDepartmentById(data.departments[i])
                departmentOptions += Posting.BuildDeparmentOption(dep.DepartmentName, data.departments[i]);
            }
            postingData = "";
            postingManager.GetApplicationsFromDepartment(depart, function(apps){
               
                    for(i=0;i<apps.length;i++)
                    {
                        current = apps[i];
                        submitTime = moment(current.PostingTime);
                        submitTime.format('MMM Do H:m A z');
                        actionTime = moment(current.LastActionTime);
                        actionTime.format('MMM Do H:m A z');
                        postingData += Posting.BuildViewResult(current._id, current.Submitter, current.JobName, current.DepartmentName, submitTime, actionTime, Posting.BuildViewStatus(current.Status));
                    
                    }
                    sideNav = Core.BuildNav(data.permissions);
                    page = Posting.BuildViewPage(departmentOptions,postingData);
                    page = Core.BuildPage("Job Applications", data.username, data.pfp, page, sideNav,"");
                    res.send(page);
                
            });
        });
    });
});



router.get('/application/:submissionId', (req,res)=>{
    //ßdepart = req.params.departmentId;
    subm = req.params.submissionId;
    //console.log("kys");
    //console.log("hi")
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            //First we need to get the application data
            postingManager.GetUserApplication(subm, async (appl) => {
                comments = ""
                //console.log(appl);
                for (i=0;i<appl.Comments.length;i++)
                {
                    //console.log("Build");
                    current = appl.Comments[i];
                    comments += Posting.BuildComment(current.Username, current.pfp, current.text);
                }
                if (appl.Status == "SUBMITTED")
                {
                    postingManager.SetApplicationStatus(appl._id, "OPENED")
                }
                dep = await jobDataManager.Async.getDepartmentById(appl.DepartmentId);
                job = await jobDataManager.Async.getJobPostingById(appl.JobPostingId);
                questions = ""
                appl.Questions.forEach((question)=>{
                    if (!question.Answer) question.Answer = "";
                    questions += Posting.BuildQuestionAnswer(Posting.Sanitize(question.question), Posting.Sanitize(question.Answer));
                });
                //console.log(appl);
                page = Posting.BuildIndividualPosting(appl.Submitter, dep.DepartmentName, job.JobName, appl._id, questions, comments);
                sideNav = Core.BuildNav(data.permissions);
                page = Core.BuildPage("Job Applications", data.username, data.pfp, page, sideNav,"");
                res.send(page);
            });
        });
    });
});
router.post('/comment/:submissionid',urlencodedParser, (req,res)=>{
    //console.log("comment")
    subm = req.params.submissionid;
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            //First we need to get the application data
           postingManager.CreateComment(subm, req.body.comment, data.username,data.pfp);
           res.redirect("/admin/careers/application" +"/"+subm);
           res.send()
        });
    });
});



router.post('/posting/create',urlencodedParser, (req,res) =>{
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            jname = req.body.title;
            depart = req.body.department;
            questions = req.body.questions;
            desc = req.body.description;
            postingManager.CreatePosting(questions, jname, depart, desc);
            res.redirect("/admin/careers/" + depart);
            res.send()
        });
    });
});


router.post('/postings/:departmentId/delete',urlencodedParser, (req,res)=>{
    depart = req.params.departmentId;
    job = req.body.job;
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            postingManager.DeletePosting(job);
            res.redirect("/admin/careers/postings/" + depart);
            res.send()
        });
    });
});
router.post('/postings/:departmentId/vanish',urlencodedParser, (req,res)=>{
    depart = req.params.departmentId;
    job = req.body.job;
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            postingManager.Vanish(job);
            res.redirect("/admin/careers/postings/" + depart);
            res.send()
        });
    });
});
router.post('/application/:submissionId/delete', (req,res)=>{
    depart = req.params.departmentId;
    subm = req.params.submissionId;
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        //console.log(req.body.action.toUpperCase());
        
    });
});
router.post('/application/:submissionId',urlencodedParser, (req,res)=>{
    depart = req.params.departmentId;
    subm = req.params.submissionId;
    permsManager.PerformUserAuth(req, res,"D", (userId)=>{
        if (!userId) return;
        //console.log(req.body.action.toUpperCase());
        if (req.body.action == "purge")
        {
            postingManager.DeleteApplication(subm);
            res.redirect("/admin/careers/");
            res.send()
        }else
        {
            postingManager.SetApplicationStatus(subm, req.body.action.toUpperCase());
            res.redirect("/admin/careers/application/" + subm);
            res.send()
        }
        
    });
});


module.exports = router;