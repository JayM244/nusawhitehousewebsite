var express = require('express');
var router = express.Router();
const bodyParser= require('body-parser')
const multer = require('multer');
var permsManager = require('../data/permissions');
var Departments = require('../components/departments/department');
var Core = require('../components/Core');
var jobDataManager = require('../../data/careers');
var departManager = require('../data/department');
var cookieParser = require('cookie-parser');
var documents = require('../components/documents/document');
var docManager = require('../data/document');
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var fs = require('fs');
router.use(urlencodedParser);

var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'uploads')
    },
    filename: function (req, file, cb) {
      cb(null, uuidv4())
    }
  })
   
  var upload = multer({ storage: storage })

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  accessKeyId: "AKIAJSK5PH23ITBOQDMQ",
  secretAccessKey: "rjYtgqLbqsW9jS9AXGirhMQsOIBPxVfU9x0IP59z"
});
router.use(cookieParser());
function uuidv4() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  }



router.get('/', (req,res)=>{
    permsManager.PerformUserAuth(req,res,"B", (id) =>{
        if (!id) {
            permsManager.PerformUserAuth(req,res,"C", (id) =>{
                if (!id) return;
                permsManager.GetUserData(id, (data)=>{
                    docManager.getDocuments((docs)=>{
                        rendered = ""
                        for (i=0;i<docs.length;i++)
                        {
                            current = docs[i];
                            rendered += documents.BuildListResult(current._id, current.Name, current.Submitter, current.Category)
                        }
                        page = documents.BuildList(rendered);
                        sideNav = Core.BuildNav(data.permissions);
                        page = Core.BuildPage("Documents", data.username, data.pfp, page, sideNav,"");
                        res.send(page);
                    })
                })
            });
        }
        permsManager.GetUserData(id, (data)=>{
            docManager.getDocuments(async (docs)=>{
                rendered = ""
                for (i=0;i<docs.length;i++)
                {
                    current = docs[i];
                    cat = await docManager.getCategory(current.Category);
                    rendered += documents.BuildListResult(current._id, current.Name, current.Submitter, cat.Name);
                }
                page = documents.BuildList(rendered);
                sideNav = Core.BuildNav(data.permissions);
                page = Core.BuildPage("Documents", data.username, data.pfp, page, sideNav,"");
                res.send(page);
            })
        })
        
        
    });
});
router.post('/',urlencodedParser, (req,res)=>{
    permsManager.PerformUserAuth(req, res,"C", (userId)=>{
        if (!userId)
        {
            permsManager.PerformUserAuth(req, res,"C", (userId)=>{
                if (!userId) return;
                docManager.deleteDocument(req.body.document);
                res.redirect("/admin/documents/");
                res.send()
            });
        }else
        {
            docManager.deleteDocument(req.body.document);
            res.redirect("/admin/documents/");
            res.send()
        }
    });
});


router.get('/create', (req,res)=>{
    permsManager.PerformUserAuth(req, res,"C", (userId)=>{
        if (!userId) {
            permsManager.PerformUserAuth(req, res,"B", (userId)=>{
                if (!userId) return;
                permsManager.GetUserData(userId, (data)=>{
                    catss = ""
                    docManager.getCategories(userId, (cats) =>{
                        catss = ""
                        for(i=0;i<cats.length;i++)
                        {
                           
                            catss += documents.BuildCategory(cats[i].Name, cats[i]._id);
                        }
                        page = documents.BuildCreator(cats);
                        sideNav = Core.BuildNav(data.permissions);
                        page = Core.BuildPage("Create Document", data.username, data.pfp, page, sideNav,"<script src='/admin/js/shared/dropify.js'></script>");
                        res.send(page);
                    });
                });
            });
        } else{
        permsManager.GetUserData(userId, (data)=>{
            catss = ""
            docManager.getCategories(userId, (cats) =>{
                catss = ""
                for(i=0;i<cats.length;i++)
                {
                           
                    catss += documents.BuildCategory(cats[i].Name, cats[i]._id);
                }
                page = documents.BuildCreator(catss);
                sideNav = Core.BuildNav(data.permissions);
                page = Core.BuildPage("Create Document", data.username, data.pfp, page, sideNav,"<script src='/admin/js/shared/dropify.js'></script>");
                res.send(page);
            });
        });
    }
    });

});

//router.use(upload.any())

router.post('/create',upload.fields([{name:"pdf"}, {name:"cover_image"}]), (req,res) => {
    permsManager.PerformUserAuth(req, res,"C", (userId)=>{
        if (!userId) {};
        permsManager.GetUserData(userId, (data)=>{
            dname = req.body.title;
            ddesc = req.body.description;
            cat = req.body.category;
            vis = req.body.visible;
            pdf_uuid = uuidv4();
            img_uuid = uuidv4();
            if (Object.keys(req.files).length == 0){
                res.redirect('/admin/documents/');
                res.send();
                return;
            }
            
            fs.readFile(req.files.pdf[0].path, (err,dta)=>{
                var pdf_params = {
                    Bucket: 'nusawhitehousedocuments', // pass your bucket name
                    Key: 'documents/' + pdf_uuid, 
                    Body: dta,
                    ACL:"public-read"
                };
                s3.upload(pdf_params, (ec,rr)=>{
                    pdf_Location = rr.Location;
                    fs.readFile(req.files.cover_image[0].path, (e,d)=>{
                        var img_params = {
                            Bucket: 'nusawhitehousedocuments', // pass your bucket name
                            Key: 'assets/' + img_uuid, 
                            Body: d,
                            ACL:"public-read"
                        };
                        s3.upload(img_params, (a,b)=>{
                            image_location = b.Location;
                            fs.unlink(req.files.pdf[0].path, (e)=>{})
                            fs.unlink(req.files.cover_image[0].path, (e)=>{})
                            docManager.createDocument(dname, data.username, ddesc, image_location, cat, pdf_Location, vis);
                            res.redirect('/admin/documents/')
                            res.send();
                        })
                    });
                })
            })
        })
    });
})

module.exports = router;