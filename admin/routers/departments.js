var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var permsManager = require('../data/permissions');
var Departments = require('../components/departments/department');
var Core = require('../components/Core');
var jobDataManager = require('../../data/careers');
var departManager = require('../data/department');
var cookieParser = require('cookie-parser');

router.use(cookieParser());
urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get('/create', (req,res) =>{
    permsManager.PerformUserAuth(req, res,"G", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            page = Departments.BuildDepartmentCreator();
            sideNav = Core.BuildNav(data.permissions);
            page = Core.BuildPage("Create Department", data.username, data.pfp, page, sideNav,"");
            res.send(page);
        });
    });
});

router.post('/create', urlencodedParser, (req,res) =>{
    permsManager.PerformUserAuth(req, res,"G", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            name = req.body.dname;
            logo = req.body.dlogo;
            desc = req.body.ddesc;
            departManager.CreateDepartment(name,logo,desc);
            res.redirect("/admin/departments");
            res.send();
        });
    });
});



router.get('/', (req,res)=>{
    permsManager.PerformUserAuth(req, res,"G", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            
            jobDataManager.getDepartments((departs)=>{
                dloaded = ""
                departs.forEach((department)=>{
                    dloaded += Departments.BuildDepartmentResult(department.DepartmentName, department._id);
                });
                dloaded = Departments.BuildDepartmentViewer(dloaded);
                sideNav = Core.BuildNav(data.permissions);
                dloaded = Core.BuildPage("View Departments", data.username, data.pfp, dloaded, sideNav,"");
                res.send(dloaded);
            });
        });
    });
});

router.post('/' , urlencodedParser, (req,res)=>{
    permsManager.PerformUserAuth(req, res,"G", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            id = req.body.department;
            departManager.DeleteDepartment(id);
            res.redirect("/admin/departments");
            res.send();
        });
    });
});

module.exports = router;