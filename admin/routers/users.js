var express = require('express');
var router = express.Router();
var permsManager = require('../data/permissions');
var Core = require('../components/Core');
var Misc = require('../components/misc/misc_draw');
var User = require('../components/users/users');
var bodyParser = require('body-parser');
var postingManager = require('../../data/careers');
var departmentManager = require('../data/department');
var careerManager = require('../../data/careers');
var cookieParser = require('cookie-parser');

router.use(cookieParser());
roles = {
    A:"Developer",
    B:"Press",
    C:"Policy Advisor",
    D:"Job Recruiter",
    E:"Cabinet",
    F:"EBB Viewer",
    G:"WHCOS",
    H:"POTUS/VPOTUS"
}
function acronym(words)
{
    if (!words) { return ''; }

    var first_letter = function(x){ if (x) { return x[0]; } else { return ''; }};

    return words.split(' ').map(first_letter).join('');
}
urlencodedParser = bodyParser.urlencoded({ extended: false });

router.get('/', (req,res)=>{
    permsManager.PerformUserAuth(req, res,"E", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            user_data = ""

            
            permsManager.GetUsers(async (ux) =>{
                //console.log(ux);
                for (j=0;j<ux.length;j++)
                {

                    user = ux[j];
                    dpts = ""
                    for(i=0;i<user.departments.length;i++)
                    {
                        department = await postingManager.Async.getDepartmentById(user.departments[i]);
                        dpts += acronym(department.DepartmentName) + ", ";
                        if (i%3 == 0){
                            dpts += "<br/>";
                        }
                    }
                    jobs = "";
                    for(i=0;i<user.permissions.length;i++)
                    {
                        //console.log(user.permissions);
                        jobs += `${roles[user.permissions.charAt(i)]}, `;
                    }
                    //console.log("added " + user.username);
                    user_data += User.BuildUserListResult(user._id, user.pfp, user.username, dpts, jobs);
                }
                //console.log("loaded");
                page = User.BuildUserList(user_data);
                sideNav = Core.BuildNav(data.permissions);
                page = Core.BuildPage("View Users", data.username, data.pfp, page, sideNav,"");
                res.send(page);
            });
        });
    });
});

router.get('/create', (req,res)=>{
    permsManager.PerformUserAuth(req, res,"E", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            page = User.BuildUserCreator();
            sideNav = Core.BuildNav(data.permissions);
            page = Core.BuildPage("View Users", data.username, data.pfp, page, sideNav,"");
            res.send(page);
        });
    });
});

router.post('/create',urlencodedParser, (req,res)=>{
    permsManager.PerformUserAuth(req, res,"E", (userId)=>{
        if (!userId) return;
        username = req.body.username;
        password = req.body.password;
        permsManager.CreateUser(username, password, (id)=>{
            res.redirect('/admin/users');
            res.send();
        });
    }); 
});

router.post('/:userId',urlencodedParser, (req,res)=> {
    permsManager.PerformUserAuth(req, res,"E", (userId)=>{
        if (!userId) return;
        if (req.body.action == "departments")
        {
            //console.log(req.body);
            id = req.params.userId;
            newDeparts = [];
            departmentManager.GetDepartments((departs) =>{
                departs.forEach((department) =>{
                        if (req.body[department._id] == "true")
                        {
                            newDeparts.push(department._id);
                        }
                });
                departmentManager.UpdateUserDepartments(id, newDeparts);
                res.redirect('/admin/users/' + id);
                res.send();
            });
        }else if (req.body.action == "perms")
        {
            id = req.params.userId;
            user_perms = "";
            permset = "ABCDEFGH";
            for(i=0;i<permset.length;i++)
            {
                if (req.body[permset.charAt(i)] == "true")
                {
                    user_perms += permset.charAt(i);
                }
            }
            permsManager.UpdatePerms(id, user_perms);
            res.redirect('/admin/users/' + id);
            res.send();
        }else if (req.body.action == "delete")
        {
            permsManager.DeleteUser(req.params.userId);
            res.redirect('/admin/users');
            res.send();
        }
    }); 
});



router.get('/:userId', (req,res)=>{
    user = req.params.userId;
    permsManager.PerformUserAuth(req, res,"E", (userId)=>{
        if (!userId) return;
        permsManager.GetUserData(userId, (data)=>{
            //Get The User Data
            permsManager.GetUserById(user, (result)=>{
                //Find their departments
                //O*U
                user_departments = result.departments;
                allDepartments = [];
                user_perms = result.permissions;
                all_perms = "";
                departmentManager.GetDepartments(async (department) =>{
                    department.forEach((depart) =>{
                        hasDepartment = false;
                        for(i=0;i<user_departments.length;i++)
                        {
                            if (depart._id == user_departments.length[i])
                            {
                                hasDepartment = true;
                            }
                        }
                        if (!hasDepartment)
                        {
                            allDepartments.push(depart);
                        }
                    });
                    pstring = "ABCDEFGH";
                    for(i=0;i<pstring.length;i++)
                    {
                        if (user_perms.includes(pstring.charAt(i)) == false)
                        {
                            all_perms += pstring.charAt(i);
                        }
                    }
                    user_departs_built = ""
                    for(i=0;i<user_departments.length;i++)
                    {
                        dep = user_departments[i];
                        ddta = await careerManager.Async.getDepartmentById(dep);
                        user_departs_built += User.BuildDeparment(ddta.DepartmentName, ddta.DepartmentImage, ddta._id, true);
                        
                    }
                  
                    regular_departs = "";
                    allDepartments.forEach(dep =>{
                        //Bad fix need to redo
                        if (!user_departs_built.includes(dep.DepartmentName))
                        {
                            regular_departs += User.BuildDeparment(dep.DepartmentName, dep.DepartmentImage, dep._id, false);
                        }
                       
                    })
                    user_perms_built = "";
                    for(i=0;i<user_perms.length;i++)
                    {
                        user_perms_built += User.BuildPermission(user_perms.charAt(i), true);
                    }
                    all_perms_built = "";
                    for(i=0;i<all_perms.length;i++)
                    {
                        all_perms_built += User.BuildPermission(all_perms.charAt(i), false);
                    }
                    page = User.BuildUserPage(regular_departs, user_departs_built, all_perms_built, user_perms_built);
                    sideNav = Core.BuildNav(data.permissions);
                    page = Core.BuildPage("View User: " + result.username, data.username, data.pfp, page, sideNav,"<script src='/admin/js/basic_drag.js'></script>");
                    res.send(page);
                });

            });
        });
    });
});

module.exports = router;
