function g(id)
{
    return document.getElementById(id);
}
function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1);
      }
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    }
    return "";
}

if (getCookie("token") != "")
{
    var x = new XMLHttpRequest();
    x.open("GET", "/api/ezlogin/" +getCookie("token"), true);
    x.onreadystatechange =function(){
        if (x.readyState == 4 && x.status == 200)
        {
            res = JSON.parse(x.responseText);
            if (res.username)
            {
                token = getCookie("token");
                username = res.username;
                verifyLoadComplete();
            }
        }
    }
    x.send();
}
function a_confirm()
{
    Swal.fire({
        title: 'Are you sure?',
        text: "Once you have submitted your application you cannot submit another for the same posting! Make sure its all clean before you continue.",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, apply!'
      }).then((result) => {
          if (result.value)
          {
            g("apform").submit();
          }
        
      })
}
function verify()
{
    code = g("inputLGEx").value;
    g("verify_button").style="display:none;";
    g("verify_load_button").style="";
    var x = new XMLHttpRequest();
    x.open("GET", "/api/ezlogin/verify/" +code, true);
    x.onreadystatechange =function(){
        if (x.readyState == 4 && x.status == 200)
        {
            res = JSON.parse(x.responseText);
            if (res.status == "success")
            {
                token = res.message;
                username = res.username;
                verifyLoadComplete();
            }else
            {
                if (res.status == "fail")
                {
                    Swal.fire(
                        'Yikes!',
                        res.message,
                        'error'
                    );
                    g("verify_button").style="";
                    g("verify_load_button").style="display:none;";
                    g("inputLGEx").value = "";
                }
            }
        }
    }
    x.send();
}
token = "";
username = "";
function verifyLoadComplete()
{
    Swal.fire(
        'Verified',
        'Welcome, ' + username,
        'success'
    );
    verify_Continue();
}
function verify_Continue()
{
    g("inputLGEx").value = token;
    g("verify_username").innerText = username.toUpperCase();
    g("verify").style="display:none";
    g("application").style = "";
    g("1").setAttribute("class", "completed");
    g("1_").innerHTML = `<i class="fas fa-check"></i>`;
    g("2").setAttribute("class", "active");
}