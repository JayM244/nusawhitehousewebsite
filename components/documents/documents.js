comp = {}

comp.BuildContainer = function(content)
{
    return ` <div class="row d-flex justify-content-center">${content}</div>`
}
comp.BuildResult = function(img,title, cat_icon, cat_name, cat_color, desc, id, date, user)
{
    return ` <div class="jumbotron text-center hoverable p-4">


    <div class="row">
  

      <div class="col-md-4 offset-md-1 mx-3 my-3">

        <div class="view overlay">
          <img src="${img}" class="img-fluid" alt=>
          <a>
            <div class="mask rgba-white-slight"></div>
          </a>
        </div>
  
      </div>

      <div class="col-md-7 text-md-left ml-3 mt-3">


        <a href="#!" class="${cat_color}-text">
          <h6 class="h6 pb-1"><i class="fas fa-${cat_icon} pr-1"></i> ${cat_name} </h6>
        </a>
  
        <h4 class="h4 mb-4">${title}</h4>
  
        <p class="font-weight-normal">${desc}</p>
        <p class="font-weight-normal">by <a><strong>${user}</strong></a>, ${date}</p>
  
        <a class="btn btn-primary" href="/documents/${id}">Read more</a>
      </div>
    </div>
  </div>`
}
comp.BuildDocumentPage = function(link)
{
    return `<div class="row d-flex justify-content-center">
    <br/>
    <div class="row">
    <object data="${link}" style="width: 1000px; height:  950px;"  type="application/pdf">
        <iframe src="https://docs.google.com/viewer?url=${link}&embedded=true" style="width: 1000px; height:  800px;" frameborder="0"></iframe>
    </object>
       
  </div>
</div>`
}
module.exports = comp;