ebb = {}
ebb.BuildLookup = () =>{
    return `<div id="verify" class="row justify-content-center">
      <div class="col-md-8">
              <form class="text-center border border-light p-5" action="" method="post">
    
                      <p class="h4 mb-4">EBB Lookup</p>
                  
                      <p>Enter the ROBLOX Username of the person you wish to view their records of.</p>
             
                  
                      <!-- Name -->
                      <div class="md-form form-lg">
                              <input type="text" id="inputLGEx" name="username" class="form-control form-control-lg">
                              <label for="inputLGEx">ROBLOX Username</label>
                            </div>
                  
                      <!-- Email -->
                      
                      <!-- Sign in button -->
                      <button class="btn btn-info btn-block" type="submit">Lookup</button>
                     
                  
                  </form>
      </div>    
      
      <!-- Stepers Wrapper -->
      
      <!-- /.Stepers Wrapper -->
    
    
    </div>`;
}

ebb.StatusBadge = {
    "1": `<span class="badge badge-danger">Tier 1</span>`,
    "2": `<span class="badge badge-warning">Tier 2</span>`,
    "3": `<span class="badge badge-info">Tier 3</span>`,
  }
ebb.Limits = {
    "1":"NO EB EMPLOYMENT",
    "2":"NO HIGH RANK POSITION",
    "3":"TEMPORARY"
}

ebb.BuildViewTable = function(body)
{
  return `<div id="verify" class="row justify-content-center">       
  <form class="text-center border border-light p-5">
      <table class="table table-sm">
          <thead>
            <tr>
              <th scope="col">ReferenceId</th>
              <th scope="col">Tier</th>
              <th scope="col">Limits</th>
              <th scope="col">Blacklist Date</th>
              <th scope="col">Blacklister</th>
              <th scope="col">Action</th>
            </tr>
          </thead>
          <tbody>
            ${body}
          </tbody>
        </table>
      </form>
</div>`
}
ebb.BuildViewRow =function(id, tier, date, blacklister)
{
  return `<tr>
  <th scope="row">${id}</th>
  <td>${ebb.StatusBadge[tier]}</td>
  <td>${ebb.Limits[tier]}</td>
  <td>${date.toString().toUpperCase()}</td>
  <td>${blacklister.toUpperCase()}</td>
  
  <td><button type="button" class="btn btn-warning btn-sm"><a href="/careers/5dae744474036c3d0c949610/5dae749774036c3d0c949611?special=true&type=ebbappeal&ref=${id}">Appeal</a></button></td>

</tr>`;
}
ebb.BuildEmptyRow = function()
{
  return `<tr>
  <th scope="row">No results to show.</th>
</tr>`
}
module.exports = ebb;