var Apps = {};

Apps.EZLogin = function()
{
    return `<div id="verify" class="row justify-content-center">
    <div  class="col-md-8">
            <div class="text-center border border-light p-5">

                    <p class="h4 mb-4">EZ-Login</p>
                
                    <p>This website uses EZ Login. Its a system where all you need to do to login is join the ROBLOX game listed below and enter the verification code it gives you.</p>
                
                    <p>
                        <a href="https://www.roblox.com/games/3300082391/EZ-Login" target="_blank">Get EZ-Login Code.</a>
                    </p>
                

                    <div class="md-form form-lg">
                            <input type="text" name="lgn_code" id="inputLGEx" class="form-control form-control-lg">
                            <label for="inputLGEx">EZ Login Code</label>
                          </div>

                    <button class="btn btn-info btn-block" id="verify_button" onclick="verify()" type="button">Verify</button>
                    <button class="btn btn-info btn-block" style="display:none;" id="verify_load_button" type="button" disabled>
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                Verifying...
                          </button>
                
                </div>
    </div>    
</div>
`;
}

Apps.BuildFormField =function(type, text, id)
{
    if (type == 0)
    {
        return ` <div class="md-form">
        <input type="text" id="field_${id}_id_" name="a_f_${id}" class="form-control">
        <label for="field_${id}_id_">${text}</label>
      </div>`;
    }
    if (type == 1)
    {
        return `   <div class="md-form">
        <textarea class="md-textarea form-control" name="a_f_${id}" aria-label="Answer the question"></textarea>
        <label for="field_${id}_id_">${text}</label>
      </div>`;
    }
    if (type==2)
    {
        return `<h3>${text}</h3>`;
    }
}
Apps.BuildApplicationForm =function(department, job, questions, special, extra = {})
{
    return `<div id="application" style="display:none;" class="row justify-content-center">
    <div  class="col-md-8">
            <div class="text-center border border-light p-5">

                    <p class="h4 mb-4">Application</p>
                    <p>This is the application for the job that you selected. Before going on with the application, please confirm that the details listed below are accurate. </p>
                    <p><b>Department: </b> ${department.toUpperCase()} <br/> <b>Application: </b> ${job.toUpperCase()} <br/> <b> Username: </b> <span id="verify_username" style="inline-block">SITEOWNER</span></p>
                    <p><i class="fas fa-info-circle"></i> Make sure to answer the application to the best of your knowledge and abilities. </p>
                    <input style="display: none" name="extra" id="extra" value='${JSON.stringify(extra)}'>
                    <br/>
                    ${special}
                    <br/>
                    ${questions}
                    <button class="btn btn-info btn-block" type="button" onclick="a_confirm()">Submit</button>
                  
                
                </div>
    </div> `;
  
}
Apps.BuildSubmissionPage = function()
{
    return `<div id="submitted" class="row justify-content-center">
    <div   class="col-md-8">
            <form class="text-center border border-light p-5">

                    <h2>Thanks!</h2>
                    <h3>Your application has been submitted.</h3>
                    <p>
                        Your appication has been sent over to the staff to be reviewed. You can check the status of your application <a href="/careers/me">here.</a> <hr/>You may be contacted by our staff at <b>any time</b> to do an interview.
                       
                    </p>
                </form>
    </div> </div>   `;
};
Apps.BuildFailurePage = function(reason)
{
    return `<div id="submitted" class="row justify-content-center">
    <div   class="col-md-8">
            <form class="text-center border border-light p-5">

                    <h1>Yikes...</h1>
                    <h5>Your application was not able to be processed.</h5>
                    <p>
                        <b>Reason</b>: <span>${reason}</span>
                    </p>
                    
                </form>
    </div> </div>   `;
};
Apps.BuildStepper = function()
{
    return `
           
    <div class="col-md-8">
            <ul class="stepper stepper-horizontal">

                    <!-- First Step -->
                    <li id="1" class="active">
                      <a href="#!">
                        <span class="circle" id="1_">1</span>
                        <span class="label">Login & Verify</span>
                      </a>
                    </li>
              
                    <!-- Second Step -->
                    <li id="2" class="">
                      <a href="#!">
                        <span class="circle"id="2_" >2</span>
                        <span class="label">Complete Application</span>
                      </a>
                    </li>
              
                    <!-- Third Step -->
                    <li id="3" class="">
                      <a href="#!">
                        <span class="circle" id="3_">3</span>
                        <span class="label">Submit</span>
                      </a>
                    </li>
              
                  </ul>
    </div>
`;
}
Apps.BuildStepper_Complete = function()
{
    return `
           
    <div class="col-md-8">
            <ul class="stepper stepper-horizontal">

                    <!-- First Step -->
                    <li id="1" class="completed">
                      <a href="#!">
                        <span class="circle" id="1_"><i class="fas fa-check"></i></span>
                        <span class="label">Login & Verify</span>
                      </a>
                    </li>
              
                    <!-- Second Step -->
                    <li id="2" class="completed">
                      <a href="#!">
                        <span class="circle"id="2_" ><i class="fas fa-check"></i></span>
                        <span class="label">Complete Application</span>
                      </a>
                    </li>
              
                    <!-- Third Step -->
                    <li id="3" class="completed">
                      <a href="#!">
                        <span class="circle" id="3_"><i class="fas fa-check"></i></span>
                        <span class="label">Submit</span>
                      </a>
                    </li>
              
                  </ul>
    </div>
`;
}
Apps.BuildStepper_Fail = function()
{
    return `
           
    <div class="col-md-8">
            <ul class="stepper stepper-horizontal">

                    <!-- First Step -->
                    <li id="1" class="completed">
                      <a href="#!">
                        <span class="circle" id="1_"><i class="fas fa-check"></i></span>
                        <span class="label">Login & Verify</span>
                      </a>
                    </li>
              
                    <!-- Second Step -->
                    <li id="2" class="completed">
                      <a href="#!">
                        <span class="circle"id="2_" ><i class="fas fa-check"></i></span>
                        <span class="label">Complete Application</span>
                      </a>
                    </li>
              
                    <!-- Third Step -->
                    <li id="3" class="warning">
                      <a href="#!">
                        <span class="circle" id="3_"><i class="fas fa-exclamation"></i></span>
                        <span class="label">Submit</span>
                      </a>
                    </li>
              
                  </ul>
    </div>
`;
}
Apps.BuildSubmissionBase =function(text, department, job)
{
    return `<script src="/js/application.js"></script><form id="apform" action="/careers/${department}/${job}" method="post">${text}</form>`;
}

Apps.BuildViewLogin =function()
{
  return `<div id="verify" class="row justify-content-center">
  <div class="col-md-8">
          <form class="text-center border border-light p-5" action="" method="post">

                  <p class="h4 mb-4">EZ-Login</p>
              
                  <p>This website uses EZ Login. Its a system where all you need to do to login is join the ROBLOX game listed below and enter the verification code it gives you.</p>
              
                  <p>
                      <a href="https://www.roblox.com/games/3300082391/EZ-Login" target="_blank">Get EZ-Login Code.</a>
                  </p>
              
                  <!-- Name -->
                  <div class="md-form form-lg">
                          <input type="text" id="inputLGEx" name="lgn_code" class="form-control form-control-lg">
                          <label for="inputLGEx">EZ Login Code</label>
                        </div>
              
                  <!-- Email -->
                  
                  <!-- Sign in button -->
                  <button class="btn btn-info btn-block" type="submit">Verify</button>
                 
              
              </form>
  </div>    
  
  <!-- Stepers Wrapper -->
  
  <!-- /.Stepers Wrapper -->


</div>`
}
Apps.BuildViewTable = function(body)
{
  return `<div id="verify" class="row justify-content-center">       
  <form class="text-center border border-light p-5">
      <table class="table table-sm">
          <thead>
            <tr>
              <th scope="col">SubmissionId</th>
              <th scope="col">Department</th>
              <th scope="col">Job</th>
              <th scope="col">Status</th>
              <th scope="col">Submission Date</th>
              <th scope="col">Last Action</th>
            </tr>
          </thead>
          <tbody>
            ${body}
          </tbody>
        </table>
      </form>
</div>`
}

Apps.StatusBadge = {
  ACCEPTED: `<span class="badge badge-success">ACCEPTED</span>`,
  SUBMITTED: `<span class="badge badge-primary">SUBMITTED</span>`,
  OPENED: `<span class="badge badge-info">OPENED</span>`,
  INTERVIEW: `<span class="badge badge-warning">INTERVIEW PENDING</span>`,
  DECLINED:`<span class="badge badge-danger">DECLINED</span>`
}

Apps.BuildViewRow =function(id, department, job, badge, submit, action)
{
  return `<tr>
  <th scope="row">${id}</th>
  <td>${department.toUpperCase()}</td>
  <td>${job.toUpperCase()}</td>
  <td>${Apps.StatusBadge[badge]}</td>
  <td>${submit.toString().toUpperCase()}</td>
  <td>${action.toString().toUpperCase()}</td>
</tr>`;
}
Apps.BuildEmptyRow = function()
{
  return `<tr>
  <th scope="row">No results to show.</th>
</tr>`
}

module.exports = Apps;