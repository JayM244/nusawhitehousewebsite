//All Careers Components

var Careers = {};
var Priv = {};

Careers.BuildDepartmentalContainer = function(inside)
{
return `${Priv.BuildCareerStatusAlert()}<div class="row d-flex justify-content-center">${inside}</div>`;
};
Careers.BuildButton = function(depID, id, disabled)
{
  if (disabled)
  {
    return `<button class="btn btn-danger waves-effect waves-light" disabled>This job is unavailable.</button>`;
  }else
  {
    return ` <a href="/careers/${depID}/${id}" class="btn btn-primary">Apply</a>`
  }
}
Careers.BuildDepartment = function(image, name, description, id)
{
    return `<div class="col-md-4 career-panel">
    <div class="card career-panel">

        <!-- Card image -->
        <img class="card-img-top department-image" src="${image}" alt="Card image cap">
      
        <!-- Card content -->
        <div class="card-body">
      
          <!-- Title -->
          <h4 class="card-title"><a>${name}</a></h4>
          <!-- Text -->
          <p class="card-text">${description}</p>
          <!-- Button -->
          <a href="/careers/${id}/" class="btn btn-primary">View Applications</a>
      
        </div>
      
      </div>
      
</div>`;
};

Careers.BuildJob = function(name, description, id , depID, disable)
{
    return `<div class="card w-100 jobs-panels">
    <div class="card-body">
      <h5 class="card-title">${name}</h5>
      <p class="card-text">${description}</p>
      ${Careers.BuildButton(depID, id, disable)}
    </div>
  </div>`;
}

Priv.BuildCareerStatusAlert = function()
{
    return `<div class="row content-row">
   
    <div class="alert alert-warning" role="alert">
        <b>Applied? </b> Click <a href="/careers/me" class="alert-link">here</a>. To go view your applications and application status.
      </div>
      
</div>`;
};

module.exports = Careers;