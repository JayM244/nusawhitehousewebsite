//Create all the core components
var conf = require('../Config').Config;

Core = {};

Core.BuildNavigation = function()
{
    Items = "";
    for(i = 0;i < conf.Pages.length; i++)
    {
        Items += `<li class="nav-item">
        <a class="nav-link waves-effect waves-light" href="${conf.Pages[i].Link}">${conf.Pages[i].Name}</a>
    </li>`
    }
    final = `<nav class="navbar navbar-expand-lg navbar-dark primary-color-dark">
    <a class="navbar-brand" href="#">The White House</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            ${Items}
        </ul>
        <span class="navbar-text white-text">
            (This is a roleplay website)
          </span>
    </div>
</nav>`;
return final;
    
}

Core.BuildBody = function(assets)
{
    return `<body><div class="container"><div class="row d-flex justify-content-center">${assets}</div></div></body>`;
}


Core.Data = ""
Core.Title = conf.Title;
Core.BuildMeta = function(title)
{
    final = `<!DOCTYPE html>
    <html lang="en">
    
    <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta http-equiv="x-ua-compatible" content="ie=edge">
      <title>[nUSA] The White House - ${title}</title>
      <!-- Font Awesome -->
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
      <!-- Bootstrap core CSS -->
      <link href="https://d3iv4xdecabd7n.cloudfront.net/bootstrap.min.css" rel="stylesheet">
      <!-- Material Design Bootstrap -->
      <link href="https://d3iv4xdecabd7n.cloudfront.net/mdb.min.css" rel="stylesheet">
      <!-- Your custom styles (optional) -->
      <link href="/css/style.css" rel="stylesheet">
    </head>`;
    return final;
}

Core.BuildPageHeader = function(title){
    return `<div class="row d-flex justify-content-center page-header">
    <div class="row"><h1> ${title} </h1></div>
  </div>`;

}

Core.BuildFooter = function()
{
    return `  <script type="text/javascript" src="https://d3iv4xdecabd7n.cloudfront.net/jquery-3.4.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="https://d3iv4xdecabd7n.cloudfront.net/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="https://d3iv4xdecabd7n.cloudfront.net/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="https://d3iv4xdecabd7n.cloudfront.net/mdb.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    `;
}

module.exports = Core;